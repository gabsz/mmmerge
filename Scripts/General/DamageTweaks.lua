local LogId = "DamageTweaks"
local Log = Log
Log(Merge.Log.Info, "Init started: %s", LogId)

local asmpatch, hookalloc, nop, u4 = mem.asmpatch, mem.hookalloc, mem.nop, mem.u4

-- Monster attack type
-- Fix Energy and Elec melee attack types
asmpatch(0x45251F, [[
call absolute 0x4DB94E
pop ecx
cmp eax, 0x6C
jnz @ener
push 1
jmp absolute 0x452508
@ener:
cmp eax, 0x6E
jnz @earth
push 0xC
jmp absolute 0x452508
@earth:
]])

nop(0x452524, 1)

-- Fix monster spell projectile attack type
asmpatch(0x404D97, [[
mov eax, dword ptr [ebp+0xC]
mov byte ptr [ebp-0x1F], al
call absolute 0x455B09
]])
nop(0x404E45, 4)
nop(0x405895, 4)

-- Fix Cyclops Projectile object
local NewCode = hookalloc(0xB)
nop(NewCode, 0xB)
asmpatch(NewCode, [[
mov word ptr [ebp-0x74], 0x235
jmp absolute 0x404BA9
]], 0xB)

mem.IgnoreProtection(true)
u4[0x404D5F] = NewCode
mem.IgnoreProtection(false)

asmpatch(0x46A72D, [[
cmp eax, 0x41
jz absolute 0x46AB1E
cmp eax, 0x3C
ja absolute 0x46B6A5
]])

-- Fix monster Rock and FireAr missile attack
local rock, firear = mem_cstring("Rock"), mem_cstring("FireAr")
mem.asmpatch(0x452753, [[
test eax, eax
pop ecx
pop ecx
jz @end
push ]] .. rock .. [[;
push esi
call absolute 0x4DA920
pop ecx
pop ecx
test eax, eax
jnz @firear
push 0xC
jmp absolute 0x45275B
@firear:
push ]] .. firear .. [[;
push esi
call absolute 0x4DA920
pop ecx
pop ecx
test eax, eax
jz absolute 0x45267B
jmp absolute 0x45275C
@end:
]])

Log(Merge.Log.Info, "Init finished: %s", LogId)

