
local CreateButton, CreateIcon, Unload = CustomUI.CreateButton, CustomUI.CreateIcon, CustomUI.UnloadIcon
local SlJadam, SlEnroth, SlAntagrich, SlBackground

 -- Disable mm8 intro. Show it after continent selection.

 mem.nop(0x4a7bf3, 6)
 mem.asmpatch(0x4a7bf3, "jmp absolute 0x4a7c45")

 -- Process "Continent settings.txt"

local function ProcessContSets()

	local TxtTab = io.open("Data/Tables/Continent settings.txt", "r")
	if not TxtTab then
		Game.ContinentSettings = {[1] = {}, [2] = {}, [3] = {}, [4] = {}}

		Game.DeathMaps =	{{[1] = {n = "out01.odm", X = 3560, Y = 7696, Z = 544, Dir = 0},		[2] = {n = "out02.odm", X = 10219, Y = -15624, Z = 265, Dir = -12}},

							{[1] = {n = "7out01.odm", X = 12552, Y = 800, Z = 193, Dir = 512},		[2] = {n = "7out02.odm", X = -16832, Y = 12512, Z = 372, Dir = 0}},

							{[1] = {n = "oute3.odm", X = -9728, Y = -11319, Z = 160, Dir = 512}, 	[2] = {n = "oute3.odm", X = -9728, Y = -11319, Z = 160, Dir = 512}}}
		return
	end

	local ContSets = {}
	local DeathMaps = {}
	local LineIt = TxtTab:lines()

	LineIt() -- skip header
	for line in LineIt do
		local Words = string.split(line, "\9")

		table.insert(ContSets, {
			UseRep		= (tonumber(Words[4]) or tonumber(Words[5])) and true or Words[6] == "x",
			RepCont		= tonumber(Words[3]),
			RepGuards	= tonumber(Words[4]),
			RepShops	= tonumber(Words[5]),
			RepNPC		= Words[6] == "x",
			RepDecMult	= tonumber(Words[7]),
			RepDecFlat	= tonumber(Words[8]),
			RepDecMin	= tonumber(Words[9]),
			ProfNews	= Words[10] == "x",
			NPCFollowers	= Words[11] == "x",
			Saturation	= tonumber(Words[12]),
			Softness	= tonumber(Words[13]),
			DeathMovie	= Words[14],
			Water		= string.split(string.replace(Words[15], " ", ""), ","),
			Skies		= string.split(string.replace(Words[26], " ", ""), ","),
			LoadingPics	= string.split(string.replace(Words[27], " ", ""), ","),
			RepTradeMult	= tonumber(Words[28]),
			RepDonateCMult	= tonumber(Words[29]),
			RepDonateMod	= tonumber(Words[30]),
			RepDonateLim	= tonumber(Words[31]),
			RepDonateSpells	= tonumber(Words[32]),
			RepRanks	= tonumber(Words[33]),
			RepPenKillPeas	= tonumber(Words[34]),
			RepPenKillGuard	= tonumber(Words[35]),
			FineKillPeasFlat	= tonumber(Words[36]),
			FineKillGuardFlat	= tonumber(Words[37]),
			FineKillPeasMult	= tonumber(Words[38]),
			FineKillGuardMult	= tonumber(Words[39]),
			RepFineKillPeasMult	= tonumber(Words[40]),
			RepFineKillGuardMult	= tonumber(Words[41]),
			RepPenStealSShop	= tonumber(Words[42]),
			RepPenStealSPeas	= tonumber(Words[43]),
			RepPenStealSGuard	= tonumber(Words[44]),
			RepPenStealUShop	= tonumber(Words[45]),
			RepPenStealUPeas	= tonumber(Words[46]),
			RepPenStealUGuard	= tonumber(Words[47]),
			RepPenStealCShop	= tonumber(Words[48]),
			RepPenStealCPeas	= tonumber(Words[49]),
			RepPenStealCGuard	= tonumber(Words[50]),
			FineStealCShop	= tonumber(Words[51]),
			FineStealCPeas	= tonumber(Words[52]),
			FineStealCGuard	= tonumber(Words[53])
		})

		table.insert(DeathMaps, {
			[1] = {n = Words[16], X = tonumber(Words[17]) or 0, Y = tonumber(Words[18]) or 0, Z = tonumber(Words[19]) or 0, Dir = tonumber(Words[20]) or 0},
			[2] = {n = Words[21], X = tonumber(Words[22]) or 0, Y = tonumber(Words[23]) or 0, Z = tonumber(Words[24]) or 0, Dir = tonumber(Words[25]) or 0}
		})
	end

	io.close(TxtTab)
	Game.ContinentSettings = ContSets
	Game.DeathMaps = DeathMaps

end
ProcessContSets()

 --	Menu
local SelectionStarted = false
local FromScreen = 0
function events.GameInitialized2()
	local function MOStd()
		Game.PlaySound(12100)
	end

	local function SlChosen(StartMap, Continent)
		if StartMap then
			local Intros = {"intro", "7intro", "6intro"}
			Game.NewGameMap = StartMap
			TownPortalControls.SwitchTo(Continent)
			if FromScreen == 0 then -- Main menu
				Game.CurrentScreen = 0
				evt.ShowMovie{1, 0, Intros[Continent]}
				mem.u4[0x6ceb24] = 1 -- new game menu action
				mem.u4[0x51e330] = 1 -- action in queue flag
			else
				Game.CurrentScreen = 21
				DoGameAction(124, 0, 0, true) -- Start new game
			end
		else
			Game.CurrentScreen = FromScreen
		end
	end

	-- Setup special screen for interface manager
	local ChooseContinentScreen = 97
	const.Screens.ChooseContinent = ChooseContinentScreen
	CustomUI.NewScreen(ChooseContinentScreen)

	SlBackground	= CreateIcon{Icon = "SlBackgr",
							Condition = function()
								if Keys.IsPressed(const.Keys.ESCAPE) then
									SlChosen()
								end
								return true
							end,
							BlockBG		= true,
							Screen		= ChooseContinentScreen,
							Layer		= 1}

	SlJadam 		= CreateButton{IconUp = "SlJadamDw", IconDown = "SlJadamUp", IconMouseOver = "SlJadamUp",
							Action = function()
								SlChosen("out01.odm", 1)
							end,
							MouseOverAction = MOStd,
							Layer		= 1,
							IsEllipse 	= true,
							Screen		= ChooseContinentScreen,
							X = 208, Y = 31}

	SlAntagrich		= CreateButton{IconUp = "SlAntagDw", IconDown = "SlAntagUp", IconMouseOver = "SlAntagUp",
							Action = function()
								SlChosen("7out01.odm", 2)
							end,
							MouseOverAction = MOStd,
							Layer		= 0,
							IsEllipse 	= true,
							Screen		= ChooseContinentScreen,
							X = 322, Y = 228}

	SlEnroth		= CreateButton{IconUp = "SlEnrothDw", IconDown = "SlEnrothUp", IconMouseOver = "SlEnrothUp",
							Action = function()
								SlChosen("oute3.odm", 3)
							end,
							MouseOverAction = MOStd,
							Layer		= 0,
							IsEllipse 	= true,
							Screen		= ChooseContinentScreen,
							X = 94, Y = 229}

end

function events.MenuAction(t)
	-- Override "New game" button original behaivor
	if t.Action == 54 and not t.Handled then
		SelectionStarted = true
		t.Handled = true
		FromScreen = 0 -- Main menu
		Game.CurrentScreen = const.Screens.ChooseContinent
		Game.PlaySound(66)
	end
end

function events.Action(t)
	-- Override "New game" button original behaivor
	if t.Action == 124 and not t.Handled and mem.u4[0x6f30c0] == 124 and Game.CurrentScreen == 1 then
		t.Handled = true
		FromScreen = 1 -- Ingame menu
		Game.CurrentScreen = const.Screens.ChooseContinent
		Game.PlaySound(66)
	end
end
