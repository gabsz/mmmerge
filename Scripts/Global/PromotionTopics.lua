--------------------------------------------
---- Learn basic blaster (Tolberti, Robert the Wise)

Game.GlobalEvtLines:RemoveEvent(950)
Game.NPCTopic[950] = Game.GlobalTxt[278] -- Blaster
evt.Global[950] = function()
	local Noone = true
	for i, v in Party do
		if v.Skills[7] == 0 and Game.Classes.Skills[v.Class][7] > 0 then
			evt.ForPlayer(i).Set{"BlasterSkill", 1}
			Noone = false
		end
	end
end

--------------------------------------------
---- Base functions
local LichAppearance = {
[const.Race.Dwarf]		= {[0] = {Portrait = 65, Voice = 26}, [1] = {Portrait = 66, Voice = 27}},
[const.Race.Dragon]		= {[0] = {Portrait = 67, Voice = 24}, [1] = {Portrait = 67, Voice = 24}},
[const.Race.Minotaur]	= {[0] = {Portrait = 69, Voice = 59}, [1] = {Portrait = 69, Voice = 59}},
default					= {[0] = {Portrait = 26, Voice = 26}, [1] = {Portrait = 27, Voice = 27}}
}

local function SetLichAppearance(i, v)
	if v.Class == const.Class.Lich then
		local Race = GetCharRace(v)

		if Merge.Settings.Conversions.PreserveRaceOnLichPromotion == 1
				and Game.Races[Race].Kind == const.RaceKind.Undead then
			if Merge.Settings.Races.MaxMaturity > 0	then
				Log(Merge.Log.Info, "Lich promotion: only improve maturity of undead kind race")
				local new_race = table.filter(Game.Races, 0,
					"BaseRace", "=", Game.Races[Race].BaseRace,
					"Family", "=", Game.Races[Race].Family,
					"Mature", "=", 1
					)[1].Id
				if new_race and new_race >= 0 then
					v.Attrs.Race = new_race
				end
			else
				Log(Merge.Log.Info, "Lich promotion: do not convert undead kind race")
			end
		elseif Merge.Settings.Conversions.PreserveRaceOnLichPromotion == 2 then
			Log(Merge.Log.Info, "Lich promotion: keep current race")
		else
			Log(Merge.Log.Info, "Lich promotion: convert race")
			local CurPortrait = Game.CharacterPortraits[v.Face]
			local CurSex = CurPortrait.DefSex

			if Game.Races[Race].Family ~= const.RaceFamily.Undead
					and Game.Races[Race].Family ~= const.RaceFamily.Ghost then
				local NewFace = LichAppearance[Game.Races[Race].BaseRace]
						or LichAppearance.default
				NewFace = NewFace[CurSex]

				local new_race

				new_race = table.filter(Game.Races, 0,
					"BaseRace", "=", Game.Races[Race].BaseRace,
					"Family", "=", const.RaceFamily.Undead,
					"Mature", "=", 	Merge.Settings.Races.MaxMaturity > 0 and 1 or 0
					)[1].Id

				if new_race and new_race >= 0 then
					v.Attrs.Race = new_race
				end

				v.Face = NewFace.Portrait
				if Merge.Settings.Conversions.KeepVoiceOnRaceConversion == 1 then
					Log(Merge.Log.Info, "Lich Promotion: keep current voice")
				else
					v.Voice = NewFace.Voice
				end
				SetCharFace(i, NewFace.Portrait)
			end

			for i = 0, 3 do
				v.Resistances[i].Base = math.max(v.Resistances[i].Base, 20)
			end
			--v.Resistances[7].Base = 65000
			--v.Resistances[8].Base = 65000

			local RepSkill = SplitSkill(v.Skills[26])
			if RepSkill > 0 then
				local CR = 0
				for i = 1, RepSkill do
					CR = CR + i
				end
				v.SkillPoints = v.SkillPoints + CR - 1
				v.Skills[26] = 0
			end
		end
	end
end

local function Promote(From, To, PromRewards, NonPromRewards, Gold, QBits, Awards)

	local Check

	if type(From) == "table" then
		Check = table.find
	else
		Check = function(v1, v2) return v1 == v2 end
	end

	for i,v in Party do
		if Check(From, v.Class) then
			evt.ForPlayer(i).Set{"ClassIs", To}
			if PromRewards then
				for k,v in pairs(PromRewards) do
					evt.ForPlayer(i).Add{k, v}
				end
			end
		elseif NonPromRewards then
			for k,v in pairs(NonPromRewards) do
				evt.ForPlayer(i).Add{k, v}
			end
		end
	end

	if GlobalRewards then
		for k,v in pairs(GlobalRewards) do
			evt.Add{k, v}
		end
	end

	if Gold then
		evt.Add{"Gold", Gold}
	end

	if QBits then
		for k,v in pairs(QBits) do
			evt.Add{"QBits", v}
		end
	end

	if Awards then
		for k,v in pairs(Awards) do
			evt.ForPlayer("All").Add{"Awards", v}
		end
	end

end

--[[
Promote2{
	From 	= ,
	To 		= ,
	PromRewards 	= {},
	NonPromRewards 	= {},
	Gold 	= ,
	QBits 	= {},
	Awards	= {},
	Reputation	 = ,
	TextIdFirst	 = ,
	TextIdSecond = ,
	TextIdRefuse = ,
	Condition	 = nil -- function() return true end
}
]]
local function Promote2(t)

	local Check

	if type(t.From) == "table" then
		Check = table.find
	else
		Check = function(v1, v2) return v1 == v2 end
	end

	local FirstTime = true
	for k,v in pairs(t.QBits) do
		if Party.QBits[v] then
			FirstTime = false
			break
		end
	end

	local CanPromote = not FirstTime or not t.Condition or t.Condition()

	if not CanPromote then
		Message(Game.NPCText[t.TextIdRefuse])
		return 0
	end

	if t.TextIdFirst then
		if FirstTime then
			Message(Game.NPCText[t.TextIdFirst])
		else
			Message(Game.NPCText[t.TextIdSecond or t.TextIdFirst])
		end
	end

	for i,v in Party do
		if Check(t.From, v.Class) then
			evt.ForPlayer(i).Set{"ClassIs", t.To}
			if t.PromRewards then
				for k,v in pairs(t.PromRewards) do
					evt.ForPlayer(i).Add{k, v}
				end
			end
		elseif FirstTime and t.NonPromRewards then
			for k,v in pairs(t.NonPromRewards) do
				evt.ForPlayer(i).Add{k, v}
			end
		end
	end

	if FirstTime then

		for k,v in pairs(t.QBits) do
			evt.Add{"QBits", v}
		end

		if t.Gold then
			evt.Add{"Gold", t.Gold}
		end

		if t.Reputation then
			evt.Add{"Reputation", t.Reputation}
		end

		if t.Awards then
			for k,v in pairs(t.Awards) do
				evt.ForPlayer("All").Add{"Awards", v}
			end
		end

	end

	return FirstTime and 1 or 2

end

local function CheckPromotionSide(ThisSideBit, OppSideBit, ThisText, OppText, ElseText)
	if Party.QBits[ThisSideBit] then
		Message(Game.NPCText[ThisText])
		return true
	elseif Party.QBits[OppSideBit] then
		Message(Game.NPCText[OppText])
	else
		Message(Game.NPCText[ElseText])
	end
	return false
end
--------------------------------------------
---- 		ENROTH PROMOTIONS			----
--------------------------------------------

--------------------------------------------
---- Enroth Knight promotion
-- First
Game.GlobalEvtLines:RemoveEvent(1378)
-- "Cavaliers"
evt.Global[1378] = function()
	Party.QBits[1138] = true	-- "Get Knight's nomination from Chadwick and return to Lord Osric Temper at Castle Temper."
	if CheckClassInParty(const.Class.Knight) then
		evt.SetMessage{Str = 1771}         -- "I am the Armsmaster in charge of the promotion of all knights in Enroth.  Worthy knights can be promoted to cavaliers.  Cavaliers embody the spirit of action and the power of purpose.  Behind their courageous example, armies charge into battle and enemies flee in terror. However, to become a cavalier, you need to be nominated by a person of authority.  If you could get the nomination of a cavalier, that would be good enough for me.   "
	else
		evt.SetMessage{Str = 1772}         -- "I am the Armsmaster in charge of the promotion of knights.  Unfortunately, none of you are knights, and therefore the skills I would teach you would be worthless.  However, if you so desire, you can attempt the task of a would-be cavalier.  Seek the nomination of a person of authority, a cavalier, for example, and return to me."
	end
	evt.SetNPCTopic{NPC = 792, Index = 0, Event = 1379}         -- "Chadwick Blackpoole" : "Nomination"
	evt.SetNPCTopic{NPC = 791, Index = 1, Event = 1381}         -- "Osric Temper" : "Cavaliers"
end

Game.GlobalEvtLines:RemoveEvent(1382)
evt.Global[1382] = function()

	Message(Game.NPCText[1776])

	if Party.QBits[1643] or Party.QBits[1644] then
		Promote(const.Class.Knight, const.Class.Cavalier, {Experience = 15000})
		if Party.QBits[1139] or Party.QBits[1645] then
			evt.SetNPCTopic{791, 2, 1384}
		end
	else
		Promote(const.Class.Knight, const.Class.Cavalier,
				{Experience = 15000},
				{Experience = 15000},
				nil,
				{1643, 1644})

		evt.Subtract{"Reputation", 5}
		evt.Subtract{"QBits", 1138}
		evt.SetNPCTopic{791, 2, 1383}
		evt.SetNPCTopic{792, 0, 1380}
	end
end

-- Second
Game.GlobalEvtLines:RemoveEvent(1383)
evt.Global[1383] = function()
	Message(Game.NPCText[1777])
	Party.QBits[1139] = true
	evt.SetNPCTopic{791, 2, 1384}
end

Game.GlobalEvtLines:RemoveEvent(1384)
evt.Global[1384] = function()

	if evt.ForPlayer("All").Cmp{"Inventory", 2128} then
		Message(Game.NPCText[1779])
		Promote(const.Class.Cavalier, const.Class.Champion,
			{Experience = 30000},
			{Experience = 30000},
			nil,
			{1645, 1646})

		evt.Subtract{"Reputation", 10}
		evt.Subtract{"QBits", 1139}

		evt.ForPlayer("All").Subtract{"Inventory", 2128}
		evt.Subtract{"QBits", 1211}

	elseif Party.QBits[1645] or Party.QBits[1646] then
		Message(Game.NPCText[1812])
		Promote(const.Class.Cavalier, const.Class.Champion, {Experience = 30000})

	else
		Message(Game.NPCText[1778])
	end

end

--------------------------------------------
---- Enroth Sorcerer promotion
-- First
Game.GlobalEvtLines:RemoveEvent(1368)
-- "Wizards"
evt.Global[1368] = function()
	Party.QBits[1135] = true         -- "Drink from the Fountain of Magic and return to Lord Albert Newton in Mist."
	if CheckClassInParty(const.Class.Sorcerer) then
		Message(Game.NPCText[1759])         -- "I am the magus in charge of training and promoting sorcerers.  Though powerful, sorcerer is not the pinnacle of the elemental magician.  Wizards take the knowledge and ability of a sorcerer, and hone those abilities to a sharper level.  They have greater magical potential than the sorcerer, and demand greater respect.  The way to earn this training, however, is not easy.  Find the Fountain of Magic, drink from its waters, and return here.  If you are able to do this, I will train you in the ways of the wizard."
	else
		Message(Game.NPCText[1760])         -- "I am the magus responsible for  the promotion of sorcerers.  Though none of you are sorcerers, I’m a fair man.  If you are able to locate the Fountain of Magic, I’ll grant you honorary wizard status.  You won’t receive any benefits of the wizards’ training, but you will gain more respect in the eyes of sorcerers and wizards.  "
	end
	evt.SetNPCTopic{NPC = 790, Index = 1, Event = 1369}         -- "Albert Newton" : "Wizards"
end

Game.GlobalEvtLines:RemoveEvent(1371)
evt.Global[1371] = function()

	Message(Game.NPCText[1762])

	if Party.QBits[1639] or Party.QBits[1640] then
		Promote(const.Class.Sorcerer, const.Class.Wizard, {Experience = 15000})
		if Party.QBits[1136] or Party.QBits[1641] then
			evt.SetNPCTopic{790, 2, 1373}
		end
	else
		Promote(const.Class.Sorcerer, const.Class.Wizard,
				{Experience = 15000},
				{Experience = 15000},
				nil,
				{1639, 1640})

		evt.Subtract{"Reputation", 5}
		evt.Subtract{"QBits", 1135}
		evt.SetNPCTopic{790, 2, 1372}
	end

end

-- Second
Game.GlobalEvtLines:RemoveEvent(1372)
evt.Global[1372] = function()
	Message(Game.NPCText[1763])
	Party.QBits[1136] = true
	evt.SetNPCTopic{790, 2, 1373}
end

Game.GlobalEvtLines:RemoveEvent(1373)
evt.Global[1373] = function()

	if evt.ForPlayer("All").Cmp{"Inventory", 2077} then
		Message(Game.NPCText[1765])
		Promote(const.Class.Wizard, const.Class.MasterWizard,
			{Experience = 30000},
			{Experience = 30000},
			nil,
			{1641, 1642})

		evt.Subtract{"Reputation", 10}
		evt.Subtract{"QBits", 1136}

		evt.ForPlayer("All").Subtract{"Inventory", 2077}
		evt.Subtract{"QBits", 1210}

	elseif Party.QBits[1641] or Party.QBits[1642] then
		Message(Game.NPCText[1813])
		Promote(const.Class.Wizard, const.Class.MasterWizard, {Experience = 30000})

	else
		Message(Game.NPCText[1764])
	end

end

--------------------------------------------
---- Enroth Archer promotion
-- First
Game.GlobalEvtLines:RemoveEvent(1405)
evt.Global[1405] = function()

	if Party.QBits[1655] or Party.QBits[1656] then
		Message(Game.NPCText[1803])
		Promote(const.Class.Archer, const.Class.WarriorMage, {Experience = 15000})
		if Party.QBits[1146] or Party.QBits[1657] then
			evt.SetNPCTopic{800, 2, 1413}
		end
	elseif evt.ForPlayer("All").Cmp{"Inventory", 2106} then
		Message(Game.NPCText[1803])
		Promote(const.Class.Archer, const.Class.WarriorMage,
			{Experience = 15000},
			{Experience = 15000},
			nil,
			{1655, 1656})

		evt.Subtract{"Reputation", 5}
		evt.Subtract{"QBits", 1145}

		evt.Subtract{"QBits", 1210}
		evt.SetNPCTopic{800, 2, 1406}

	else
		Message(Game.NPCText[1802])
	end

end

-- Second
Game.GlobalEvtLines:RemoveEvent(1406)
evt.Global[1406] = function()
	Message(Game.NPCText[1804])
	Party.QBits[1146] = true
	evt.SetNPCTopic{800, 2, 1413}
end

Game.GlobalEvtLines:RemoveEvent(1413)
evt.Global[1413] = function()

	if Party.QBits[1657] or Party.QBits[1658] then
		Message(Game.NPCText[1808])
		Promote(const.Class.WarriorMage, const.Class.MasterArcher, {Experience = 40000})

	elseif	Party.QBits[1180] and Party.QBits[1181] and Party.QBits[1182]
		and	Party.QBits[1183] and Party.QBits[1184] and Party.QBits[1185] then

		Message(Game.NPCText[1807])
		Promote(const.Class.WarriorMage, const.Class.MasterArcher,
			{Experience = 40000},
			{Experience = 40000},
			nil,
			{1657, 1658})

		evt.Subtract{"Reputation", 10}
		evt.Subtract{"QBits", 1146}

		evt.ForPlayer("All").Subtract{"Inventory", 2077}
		evt.Subtract{"QBits", 1210}

	else

		if not evt.ForPlayer("All").Cmp{"Inventory", 2106} then
			evt.GiveItem{1, 0, 2106}
			Mouse.Item.Identified = true
			Mouse.Item.Bonus = 0
		end

		Message(Game.NPCText[1805])
	end

end

--------------------------------------------
---- Enroth Cleric promotion
-- First
Game.GlobalEvtLines:RemoveEvent(1349)
evt.Global[1349] = function()

	if Party.QBits[1647] or Party.QBits[1648] then
		Message(Game.NPCText[1740])
		Promote(const.Class.Cleric, const.Class.Priest, {Experience = 15000})

		if Party.QBits[1131] or Party.QBits[1132] or Party.QBits[1649] or Party.QBits[1650] then
			evt.SetNPCTopic{801, 2, 1351}
		end

	elseif Party.QBits[1130] then
		Message(Game.NPCText[1740])
		Promote(const.Class.Cleric, const.Class.Priest,
				{Experience = 15000},
				{Experience = 15000},
				nil,
				{1647, 1648})

		evt.Subtract{"Reputation", 5}
		evt.Subtract{"QBits", 1129}

		evt.SetNPCTopic{801, 2, 1350}

	else
		Message(Game.NPCText[1739])
	end

end

-- Second
Game.GlobalEvtLines:RemoveEvent(1350)
evt.Global[1350] = function()
	Message(Game.NPCText[1741])
	Party.QBits[1131] = true
	evt.SetNPCTopic{801, 2, 1351}
end

Game.GlobalEvtLines:RemoveEvent(1351)
evt.Global[1351] = function()

	if Party.QBits[1649] and Party.QBits[1650] then
		Message(Game.NPCText[1745])
		Promote(const.Class.Priest, const.Class.HighPriest, {Experience = 30000})

	elseif Party.QBits[1132] then
		Message(Game.NPCText[1744])
		Promote(const.Class.Priest, const.Class.HighPriest,
				{Experience = 30000},
				{Experience = 30000},
				nil,
				{1649, 1650})

		evt.Subtract{"Reputation", 10}
		evt.Subtract{"QBits", 1131}

	elseif evt.ForPlayer("All").Cmp{"Inventory", 2054} then
		Message(Game.NPCText[1743])

	else
		Message(Game.NPCText[1742])

	end

end

--------------------------------------------
---- Enroth Druid promotion
-- First
Game.GlobalEvtLines:RemoveEvent(1678)
evt.Global[1678] = function()
	Message(Game.NPCText[1792])
	if Party.QBits[1651] or Party.QBits[1652] then
		Promote(const.Class.Druid, const.Class.GreatDruid, {Experience = 15000})
		evt.Subtract{"QBits", 1142}
		if Party.QBits[1653] then
			evt.SetNPCTopic{799, 2, 1679}
		end
	else
		Promote(const.Class.Druid, const.Class.GreatDruid,
				{Experience = 15000}, {Experience = 15000}, nil, {1651, 1652})
		evt.Subtract{"QBits", 1142}
		evt.SetNPCTopic{799, 2, 1397}
		evt.SetNPCTopic{799, 1, 1678}
		evt.SetNPCTopic{1090, 0, 0}
	end
end

Game.GlobalEvtLines:RemoveEvent(1397)
evt.Global[1397] = function()
	Message(Game.NPCText[1793])
	Party.QBits[1143] = true
	evt.SetNPCTopic{1090, 2, 1398}
end

-- Second
Game.GlobalEvtLines:RemoveEvent(1679)
evt.Global[1679] = function()
	if Party.QBits[1653] or Party.QBits[1654] then
		Message(Game.NPCText[1796])
		Promote(const.Class.GreatDruid, const.Class.ArchDruid, {Experience = 40000})
	else
		Message(Game.NPCText[1794])
		Promote(const.Class.GreatDruid, const.Class.ArchDruid,
				{Experience = 40000}, {Experience = 40000}, nil, {1653, 1654})
		Party.QBits[1198] = true
		evt.Subtract{"QBits", 1143}
		evt.Subtract{"Reputation", 10}
		evt.SetNPCTopic{799, 1, 1678}
		evt.SetNPCTopic{799, 2, 1679}
		evt.SetNPCTopic{1090, 0, 0}
	end
end

--------------------------------------------
---- Enroth Paladin promotion
-- First
Game.GlobalEvtLines:RemoveEvent(1327)
evt.Global[1327] = function()

	if Party.QBits[1635] or Party.QBits[1636] then
		Message(Game.NPCText[1713])
		evt.Subtract{"QBits", 1112}
		Promote(const.Class.Paladin, const.Class.Crusader, {Experience = 15000})
		NPCFollowers.Remove(796)
		if Party.QBits[1113] or Party.QBits[1637] then
			evt.SetNPCTopic{789, 3, 1329}
		end

	elseif Party.QBits[1699] then
		Message(Game.NPCText[1713])
		Promote(const.Class.Paladin, const.Class.Crusader,
				{Experience = 15000}, {Experience = 15000}, 5000, {1635, 1636})
		evt.Subtract{"QBits", 1699}
		evt.Subtract{"QBits", 1112}
		evt.Subtract{"Reputation", 5}
		NPCFollowers.Remove(796)
		evt.SetNPCTopic{789, 3, 1328}

	else
		Message(Game.NPCText[1712])

	end
end
-- Second
Game.GlobalEvtLines:RemoveEvent(1328)
evt.Global[1328] = function()
	Message(Game.NPCText[1714])
	Party.QBits[1113] = true
	evt.SetNPCTopic{789, 3, 1329}
end

Game.GlobalEvtLines:RemoveEvent(1329)
evt.Global[1329] = function()
	if Party.QBits[1637] or Party.QBits[1638] then
		Message(Game.NPCText[1717])
		Promote(const.Class.Crusader, const.Class.Hero, {Experience = 30000})

	elseif evt.ForPlayer("All").Cmp{"Inventory", 2075} then
		Message(Game.NPCText[1716])
		Promote(const.Class.Crusader, const.Class.Hero,
				{Experience = 30000}, {Experience = 30000}, nil, {1637, 1638})
		evt.Subtract{"Reputation", 10}
		evt.Subtract{"QBits", 1113}
		evt.Subtract{"Inventory", 2075}

	else
		Message(Game.NPCText[1715])

	end
end

--------------------------------------------
---- 		ANTAGARICH PROMOTIONS		----
--------------------------------------------

--------------------------------------------
---- Antagarich Archer promotion
-- First
Game.GlobalEvtLines:RemoveEvent(818)
evt.Global[818] = function()

	local result = Promote2{
		From 	= const.Class.Archer,
		To 		= const.Class.WarriorMage,
		PromRewards 	= {Experience = 30000},
		NonPromRewards 	= {Experience = 15000},
		Gold 	= 7500,
		QBits 	= {1584, 1585},
		Awards	= nil,
		Reputation	 = -10,
		TextIdFirst	 = 1088,
		TextIdSecond = 1088,
		TextIdRefuse = 1089,
		Condition	 = function() return Party.QBits[570] end
	}

	if result == 1 then
		evt.Subtract{"QBits", 543}
		evt.SetNPCTopic{380, 1, 819}
	end

end

-- Second good
Game.GlobalEvtLines:RemoveEvent(819)
evt.Global[819] = function()
	if Party.QBits[612] then
		evt.SetNPCTopic{380, 1, 820}
		evt.Set{"QBits", 544}

		Message(Game.NPCText[1090])
	elseif Party.QBits[611] then
		Message(Game.NPCText[1092])
	else
		Message(Game.NPCText[1091])
	end
end
Game.GlobalEvtLines:RemoveEvent(816)
evt.Global[816] = function()

	local result = Promote2{
		From 	= const.Class.WarriorMage,
		To 		= const.Class.MasterArcher,
		PromRewards 	= {Experience = 80000},
		NonPromRewards 	= {Experience = 40000},
		Gold 	= nil,
		QBits 	= {1586, 1587},
		Awards	= nil,
		Reputation	 = -10,
		TextIdFirst	 = 1085,
		TextIdSecond = 1085,
		TextIdRefuse = 1086,
		Condition	 = function() return evt.ForPlayer("All").Cmp{"Inventory", 1344} end
	}

	if result == 1 then
		evt.ForPlayer(0).Add{"Inventory", 1345}
		evt.ForPlayer("All").Subtract{"Inventory", 1344}
		evt.SetNPCGreeting{379, 172}
		Party.QBits[542] = false
	end

end

-- Second evil
Game.GlobalEvtLines:RemoveEvent(820)
evt.Global[820] = function()

	local result = Promote2{
		From 	= const.Class.WarriorMage,
		To 		= const.Class.Sniper,
		PromRewards 	= {Experience = 80000},
		NonPromRewards 	= {Experience = 40000},
		Gold 	= nil,
		QBits 	= {1588, 1589},
		Awards	= nil,
		Reputation	 = 10,
		TextIdFirst	 = 1093,
		TextIdSecond = 1093,
		TextIdRefuse = 1094,
		Condition	 = function() return evt.ForPlayer("All").Cmp{"Inventory", 1344} end
	}

	if result == 1 then
		evt.ForPlayer(0).Add{"Inventory", 1345}
		evt.ForPlayer("All").Subtract{"Inventory", 1344}
		evt.SetNPCGreeting{380, 174}
		Party.QBits[544] = false
	elseif result == 0 then
		Party.QBits[544] = true
	end

end

--------------------------------------------
---- Antagarich Cleric promotion
-- First
Game.GlobalEvtLines:RemoveEvent(839)
evt.Global[839] = function()

	local result = Promote2{
		From 	= const.Class.Cleric,
		To 		= const.Class.Priest,
		PromRewards 	= {Experience = 30000},
		NonPromRewards 	= {Experience = 15000},
		Gold 	= 5000,
		QBits 	= {1607, 1608},
		Awards	= nil,
		Reputation	 = -5,
		TextIdFirst	 = 1134,
		TextIdSecond = 1134,
		TextIdRefuse = 1135,
		Condition	 = function() return evt.ForPlayer("All").Cmp{"Inventory", 1485} end
	}

	if result == 1 then
		evt.ForPlayer("All").Subtract{"Inventory", 1485}
		evt.Subtract{"QBits", 730}
		evt.Set{"QBits", 576}
		evt.Subtract{"QBits", 555}
		evt.SetNPCTopic{386, 1, 840}
		evt.SetNPCTopic{386, 0, 839}
	elseif result == 2 then
		evt.Subtract{"QBits", 555}
	end

end

Game.GlobalEvtLines:RemoveEvent(836)
evt.Global[836] = function()
	if Party.QBits[1607] or Party.QBits[1608] then
		if Party.QBits[612] then
			Message(Game.NPCText[1130])
		elseif Party.QBits[611] then
			Message(Game.NPCText[1127])
			evt.Set{"QBits", 554}
			evt.SetNPCTopic{385, 0, 837}
		else
			Message(Game.NPCText[1128])
		end
	else
		Message(Game.NPCText[1129])
	end
end

Game.GlobalEvtLines:RemoveEvent(840)
evt.Global[840] = function()
	if Party.QBits[611] then
		Message(Game.NPCText[200])
	elseif Party.QBits[612] then
		Message(Game.NPCText[1136])
		evt.Set{"QBits", 556}
		evt.SetNPCTopic{386, 1, 841}
	else
		Message(Game.NPCText[1137])
	end
end

-- Second good
Game.GlobalEvtLines:RemoveEvent(837)
evt.Global[837] = function()
	if Party.QBits[1609] or Party.QBits[1610] then
		Message(Game.NPCText[1131])
		Promote(const.Class.Priest, const.Class.PriestLight, {Experience = 80000})
		evt.Subtract{"QBits", 554}

	elseif Party.QBits[574] then
		Message(Game.NPCText[1131])
		Promote(const.Class.Priest, const.Class.PriestLight,
				{Experience = 80000}, {Experience = 40000}, 10000, {1609, 1610})
		evt.ForPlayer("Current").Subtract{"Reputation", 10}
		evt.Subtract{"QBits", 554}
		evt.SetNPCGreeting{385, 188}

	else
		Message(Game.NPCText[1132])

	end
end

-- Second evil
Game.GlobalEvtLines:RemoveEvent(841)
evt.Global[841] = function()
	if Party.QBits[1611] or Party.QBits[1612] then
		Message(Game.NPCText[201])
		Promote(const.Class.Priest, const.Class.PriestDark, {Experience = 80000})
		evt.Subtract{"QBits", 556}

	elseif Party.QBits[575] then
		Message(Game.NPCText[201])
		Promote(const.Class.Priest, const.Class.PriestDark,
				{Experience = 80000}, {Experience = 40000}, 10000, {1611, 1612})
		evt.ForPlayer("All").Subtract{"Reputation", 10}
		evt.Subtract{"QBits", 556}
		evt.SetNPCGreeting{386, 190}

	else
		Message(Game.NPCText[1138])

	end
end

--------------------------------------------
---- Antagarich Druid promotion
-- First
Game.GlobalEvtLines:RemoveEvent(849)
evt.Global[849] = function()

	local result = Promote2{
		From 	= const.Class.Druid,
		To 		= const.Class.GreatDruid,
		PromRewards 	= {Experience = 30000},
		NonPromRewards 	= {Experience = 15000},
		Gold 	= nil,
		QBits 	= {1613, 1614},
		Awards	= nil,
		Reputation	 = -5,
		TextIdFirst	 = 1155,
		TextIdSecond = 1155,
		TextIdRefuse = 1153,
		Condition	 = function() return Party.QBits[562] end
	}

	if result == 1 then
		evt.Subtract{"QBits", 561}
		evt.SetNPCTopic{389, 1, 850}
	elseif result == 0 and (Party.QBits[563] or Party.QBits[564] or Party.QBits[565]) then
		Message(Game.NPCText[1154])
	end

end

-- Second good
Game.GlobalEvtLines:RemoveEvent(850)
evt.Global[850] = function()
	if CheckPromotionSide(611, 612, 1156, 1158, 1157) then
		evt.Set{"QBits", 566}
		evt.SetNPCTopic{389, 1, 851}
	end
end

Game.GlobalEvtLines:RemoveEvent(851)
evt.Global[851] = function()

	local result = Promote2{
		From 	= const.Class.GreatDruid,
		To 		= const.Class.ArchDruid,
		PromRewards 	= {Experience = 80000},
		NonPromRewards 	= {Experience = 40000},
		Gold 	= nil,
		QBits 	= {1615, 1616},
		Awards	= nil,
		Reputation	 = -10,
		TextIdFirst	 = 1159,
		TextIdSecond = 1159,
		TextIdRefuse = 1160,
		Condition	 = function() return Party.QBits[577] end
	}

	if result == 1 then
		evt.Subtract{"QBits", 566}
		evt.SetNPCGreeting{389, 196}
	end

end

-- Second evil
Game.GlobalEvtLines:RemoveEvent(852)
evt.Global[852] = function()
	if Party.QBits[1613] or Party.QBits[1614] then
		if CheckPromotionSide(612, 611, 1161, 1164, 1163) then
			evt.Set{"QBits", 567}
			evt.SetNPCTopic{390, 0, 853}
		end
	else
		Message(Game.NPCText[1162])
	end
end

Game.GlobalEvtLines:RemoveEvent(853)
evt.Global[853] = function()

	local result = Promote2{
		From 	= const.Class.GreatDruid,
		To 		= const.Class.Warlock,
		PromRewards 	= {Experience = 80000},
		NonPromRewards 	= {Experience = 40000},
		Gold 	= nil,
		QBits 	= {1617, 1618},
		Awards	= nil,
		Reputation	 = -10,
		TextIdFirst	 = 1165,
		TextIdSecond = 1165,
		TextIdRefuse = 1166,
		Condition	 = function() return evt.ForPlayer("All").Cmp{"Inventory", 1449} end
	}

	if result == 1 then
		evt.Subtract{"QBits", 567}
		evt.Subtract{"QBits", 739}
		evt.SetNPCGreeting{390, 198}
		evt.Set{"QBits", 1687}
		evt.ForPlayer("All").Subtract{"Inventory", 1449}
		NPCFollowers.Add(396)
	end
end

--------------------------------------------
---- Antagarich Paladin promotion
-- First
Game.GlobalEvtLines:RemoveEvent(801)
evt.Global[801] = function()
	Message(Game.NPCText[1012])
	NPCFollowers.Add(356)

	evt.Set{"QBits", 534}
	evt.Set{"QBits", 1684}

	evt.MoveNPC{356, 0}
	evt.SetNPCTopic{356, 0, 802}
end

Game.GlobalEvtLines:RemoveEvent(802)
evt.Global[802] = function()

	if Party.QBits[1590] or Party.QBits[1591] then
		Promote(const.Class.Paladin, const.Class.Crusader, {Experience = 30000})
		Message(Game.NPCText[1013])

	elseif Party.QBits[535] then

		Promote(const.Class.Paladin, const.Class.Crusader,
				{Experience = 30000},
				{Experience = 15000},
				nil,
				{1590, 1591})

		Party.QBits[534] = false
		Party.QBits[1684] = false
		evt.Subtract{"Reputation", 5}

		evt.MoveNPC{356, 941}
		evt.SetNPCTopic{356, 0, 803}
		evt.SetNPCTopic{356, 1, 802}
		evt.SetNPCGreeting{356, 158}
		NPCFollowers.Remove(356)

		Message(Game.NPCText[1013])
	else
		Message(Game.NPCText[1014])
	end

end

evt.Global[805] = function()
	if (Party.QBits[611] or Party.QBits[612]) and not (Party.QBits[1592] or Party.QBits[1594]) then
		NPCFollowers.Add(393)
	end
end
-- Second good
Game.GlobalEvtLines:RemoveEvent(803)
evt.Global[803] = function()
	if Party.QBits[611] then
		Message(Game.NPCText[1015])
		evt.Set{"QBits", 536}
		evt.SetNPCTopic{356, 0, 804}
		evt.SetNPCGreeting{356, 158}
		evt.MoveNPC{393, 1158}
	elseif Party.QBits[612] then
		Message(Game.NPCText[1016])
	else
		Message(Game.NPCText[1017])
	end
end

Game.GlobalEvtLines:RemoveEvent(804)
evt.Global[804] = function()

	if Party.QBits[1592] or Party.QBits[1593] then
		NPCFollowers.Remove(393)
		Promote(const.Class.Crusader, const.Class.Hero, {Experience = 80000})
		Message(Game.NPCText[1018])

	elseif Party.QBits[1685] then
		NPCFollowers.Remove(393)
		Promote(const.Class.Crusader, const.Class.Hero,
				{Experience = 80000},
				{Experience = 40000},
				nil,
				{1592, 1593})

		Party.QBits[536] = false
		Party.QBits[1685] = false
		evt.Subtract{"Reputation", 10}
		evt.MoveNPC{393, 941}
		evt.SetNPCGreeting{356, 161}

		NPCFollowers.Remove(393)
		Message(Game.NPCText[1018])
	else
		evt.Set{"QBits", 536}
		Message(Game.NPCText[1019])
	end

end

-- Second evil
Game.GlobalEvtLines:RemoveEvent(807)
evt.Global[807] = function()

	if Party.QBits[1594] or Party.QBits[1595] then
		NPCFollowers.Remove(393)
		Promote(const.Class.Crusader, const.Class.Villain, {Experience = 80000})
		Message(Game.NPCText[1029])

	elseif Party.QBits[1685] then
		NPCFollowers.Remove(393)
		Promote(const.Class.Crusader, const.Class.Villain,
				{Experience = 80000},
				{Experience = 40000},
				nil,
				{1594, 1595})

		Party.QBits[538] = false
		Party.QBits[1685] = false
		evt.Add{"Reputation", 10}
		evt.SetNPCGreeting{357, 165}

		NPCFollowers.Remove(393)
		Message(Game.NPCText[1029])
	else
		Message(Game.NPCText[1030])
	end

end

--------------------------------------------
---- Antagarich Monk promotion
-- First
Game.GlobalEvtLines:RemoveEvent(810)
evt.Global[810] = function()

	if Party.QBits[1572] or Party.QBits[1573] then
		Promote(const.Class.Monk, const.Class.Initiate, {Experience = 30000})
		Party.QBits[539] = false

	else
		Promote(const.Class.Monk, const.Class.Initiate,
				{Experience = 30000},
				{Experience = 15000},
				nil,
				{1572, 1573})

		Party.QBits[539] = false
		Party.QBits[1685] = false

		evt.SetNPCTopic{377, 0, 810}
		evt.SetNPCTopic{377, 1, 811}
		evt.SetNPCTopic{394, 0, 810}
		evt.SetNPCTopic{394, 1, 811}

	end

	Message(Game.NPCText[1032])
end

Game.GlobalEvtLines:RemoveEvent(811)
evt.Global[811] = function()
	if Party.QBits[611] then

		evt.Set{"QBits", 540}
		Message(Game.NPCText[1034])
		evt.SetNPCTopic{377, 1, 812}
		evt.SetNPCTopic{394, 1, 812}

	elseif Party.QBits[612] then
		Message(Game.NPCText[1035])

	else
		Message(Game.NPCText[1036])

	end
end

-- Second good
Game.GlobalEvtLines:RemoveEvent(812)
evt.Global[812] = function()

	if Party.QBits[1574] or Party.QBits[1575] then
		Party.QBits[540] = false
		Promote(const.Class.Initiate, const.Class.Master, {Experience = 80000})
		Message(Game.NPCText[1072])

	elseif Party.QBits[755] or evt.ForPlayer("All").Cmp{"Inventory", 1332} then

		Promote(const.Class.Initiate, const.Class.Master,
				{Experience = 80000},
				{Experience = 40000},
				nil,
				{1574, 1575})

		Party.QBits[540] = false
		evt.Subtract{"Reputation", 10}
		evt.SetNPCGreeting{377, 167}

		Message(Game.NPCText[1072])
	else
		Message(Game.NPCText[1073])
	end

end

-- Second evil
Game.GlobalEvtLines:RemoveEvent(814)
evt.Global[814] = function()

	if Party.QBits[1576] or Party.QBits[1577] then
		Promote(const.Class.Initiate, const.Class.Ninja, {Experience = 80000})
		Message(Game.NPCText[1080])
		Party.QBits[541] = false

	elseif Party.QBits[754] then

		Promote(const.Class.Initiate, const.Class.Ninja,
				{Experience = 80000},
				{Experience = 40000},
				nil,
				{1576, 1577})

		Party.QBits[541] = false
		evt.Subtract{"Reputation", 10}
		evt.SetNPCGreeting{378, 170}

		Message(Game.NPCText[1080])

	elseif Party.QBits[569] then
		Message(Game.NPCText[1078])

	else
		Message(Game.NPCText[1079])

	end

end

--------------------------------------------
---- Antagarich Knight promotion
-- First
Game.GlobalEvtLines:RemoveEvent(824)
evt.Global[824] = function()

	local result = Promote2{
		From 	= const.Class.Knight,
		To 		= const.Class.Cavalier,
		PromRewards 	= {Experience = 30000},
		NonPromRewards 	= {Experience = 15000},
		Gold 	= nil,
		QBits 	= {1566, 1567},
		Awards	= nil,
		Reputation	 = -5,
		TextIdFirst	 = 1102,
		TextIdSecond = 1102,
		TextIdRefuse = 1103,
		Condition	 = function() return Party.QBits[652] end
	}

	if result == 1 then
		evt.Subtract{"QBits", 546}
		evt.SetNPCTopic{382, 1, 825}
	end

end

-- Second evil
Game.GlobalEvtLines:RemoveEvent(825)
evt.Global[825] = function()
	if CheckPromotionSide(612, 611, 1104, 1106, 1105) then
		evt.Set{"QBits", 547}
		evt.SetNPCTopic{382, 1, 826}
	end
end

Game.GlobalEvtLines:RemoveEvent(826)
evt.Global[826] = function()

	local result = Promote2{
		From 	= const.Class.Cavalier,
		To 		= const.Class.BlackKnight,
		PromRewards 	= {Experience = 80000},
		NonPromRewards 	= {Experience = 40000},
		Gold 	= nil,
		QBits 	= {1570, 1571},
		Awards	= nil,
		Reputation	 = -10,
		TextIdFirst	 = 1107,
		TextIdSecond = 1107,
		TextIdRefuse = 1108,
		Condition	 = function() return Party.QBits[572] end
	}

	if result == 1 then
		evt.Subtract{"QBits", 547}
		evt.SetNPCGreeting{382, 178}
	end

end

-- Second good
Game.GlobalEvtLines:RemoveEvent(821)
evt.Global[821] = function()
	if Party.QBits[1566] or Party.QBits[1567] then
		if CheckPromotionSide(611, 612, 1095, 1097, 1096) then
			evt.Set{"QBits", 545}
			evt.SetNPCTopic{381, 0, 822}
		end
	else
		Message(Game.NPCText[1098])
	end
end

Game.GlobalEvtLines:RemoveEvent(822)
evt.Global[822] = function()

	local result = Promote2{
		From 	= const.Class.Cavalier,
		To 		= const.Class.Champion,
		PromRewards 	= {Experience = 80000},
		NonPromRewards 	= {Experience = 40000},
		Gold 	= nil,
		QBits 	= {1568, 1569},
		Awards	= nil,
		Reputation	 = -10,
		TextIdFirst	 = 1099,
		TextIdSecond = 1099,
		TextIdRefuse = 1100,
		Condition	 = function() return Party.ArenaWinsKnight >= 5 end
	}

	if result == 1 then
		evt.Subtract{"QBits", 545}
		evt.SetNPCGreeting{381, 176}
	end

end

--------------------------------------------
---- Antagarich Ranger promotion
-- First
Game.GlobalEvtLines:RemoveEvent(830)
evt.Global[830] = function()
	local result = Promote2{
		From 	= const.Class.Ranger,
		To 		= const.Class.Hunter,
		PromRewards = {Experience = 30000},
		QBits 	= {1578, 1579},
		TextIdFirst	 = 1117,
		TextIdRefuse = 1117,
		Condition	 = function() return Party.QBits[1578] or Party.QBits[1579] end
	}

	Party.QBits[549] = false

end

Game.GlobalEvtLines:RemoveEvent(833)
evt.Global[833] = function()

	local result = Promote2{
		From 	= const.Class.Ranger,
		To 		= const.Class.Hunter,
		PromRewards 	= {Experience = 30000},
		NonPromRewards 	= {Experience = 15000},
		QBits 		= {1578, 1579},
		TextIdFirst	= 1123
	}

	Party.QBits[549] = false
	evt.SetNPCTopic{384, 0, 830}
	evt.SetNPCTopic{384, 1, 831}

end

-- Second evil
Game.GlobalEvtLines:RemoveEvent(831)
evt.Global[831] = function()
	if CheckPromotionSide(612, 611, 1118, 1120, 1119) then
		evt.Set{"QBits", 550}
		evt.SetNPCTopic{384, 1, 832}
	end
end

Game.GlobalEvtLines:RemoveEvent(832)
evt.Global[832] = function()

	local result = Promote2{
		From 	= const.Class.Hunter,
		To 		= const.Class.BountyHunter,
		PromRewards 	= {Experience = 80000},
		NonPromRewards 	= {Experience = 40000},
		Gold 	= nil,
		QBits 	= {1582, 1583},
		Awards	= nil,
		Reputation	 = -10,
		TextIdFirst	 = 1121,
		TextIdSecond = 1121,
		TextIdRefuse = 1122,
		Condition	 = function() return evt.Cmp{310, 10000} end
	}

	if result == 1 then
		evt.Subtract{"QBits", 550}
		evt.SetNPCGreeting{384, 182}
	end

end

-- Second good
Game.GlobalEvtLines:RemoveEvent(827)
evt.Global[827] = function()
	if Party.QBits[1578] or Party.QBits[1579] then
		if CheckPromotionSide(611, 612, 1109, 1112, 1110) then
			evt.Set{"QBits", 548}
			evt.SetNPCTopic{383, 0, 828}
		end
	else
		Message(Game.NPCText[1111])
	end
end

Game.GlobalEvtLines:RemoveEvent(828)
evt.Global[828] = function()

	local result = Promote2{
		From 	= const.Class.Hunter,
		To 		= const.Class.RangerLord,
		PromRewards 	= {Experience = 80000},
		NonPromRewards 	= {Experience = 40000},
		Gold 	= nil,
		QBits 	= {1580, 1581},
		Awards	= nil,
		Reputation	 = -5,
		TextIdFirst	 = 1115,
		TextIdSecond = 1115,
		TextIdRefuse = 1113,
		Condition	 = function() return Party.QBits[553] end
	}

	if result == 1 then
		evt.Subtract{"QBits", 548}
		evt.SetNPCGreeting{383, 180}
	elseif result == 2 then
		Party.QBits[548] = false
	elseif result == 0 and Party.QBits[552] then
		Message(Game.NPCText[1114])
	end

end
--------------------------------------------
---- Antagarich Thief promotion
-- First
Game.GlobalEvtLines:RemoveEvent(795)
evt.Global[795] = function()

	if Party.QBits[1560] or Party.QBits[1561] then
		Promote(const.Class.Thief, const.Class.Rogue, {Experience = 15000})
		Message(Game.NPCText[995])

	elseif evt.ForPlayer("All").Cmp{"Inventory", 1426} then

		Promote(const.Class.Thief, const.Class.Rogue,
				{Experience = 30000},
				{Experience = 15000},
				5000,
				{1560, 1561})

		evt.ForPlayer("All").Subtract{"Inventory", 1426}
		evt.Subtract{"QBits", 724}
		evt.Subtract{"QBits", 530}

		evt.SetNPCTopic{354, 1, 796}
		evt.SetNPCTopic{354, 0, 795}
		Message(Game.NPCText[995])
	else
		Message(Game.NPCText[994])
	end

end

-- Second good
Game.GlobalEvtLines:RemoveEvent(796)
evt.Global[796] = function()

	if Party.QBits[611] then
		Message(Game.NPCText[998])
		evt.Set{"QBits", 531}
		evt.SetNPCTopic{354, 1, 797}
	elseif Party.QBits[612] then
		Message(Game.NPCText[996])
	else
		Message(Game.NPCText[997])
	end

end

Game.GlobalEvtLines:RemoveEvent(797)
evt.Global[797] = function()

	if Party.QBits[1562] or Party.QBits[1563] then
		Promote(const.Class.Rogue, const.Class.Spy, {Experience = 80000})
		Message(Game.NPCText[1002])

	elseif Party.QBits[532] then

		Promote(const.Class.Rogue, const.Class.Spy,
				{Experience = 80000},
				{Experience = 40000},
				15000,
				{1562, 1563})

		Party.QBits[531] = false

		evt.SetNPCGreeting{354, 154}
		Message(Game.NPCText[1002])

	elseif Party.QBits[568] then
		Message(Game.NPCText[1000])

	else
		Message(Game.NPCText[999])
	end

end

-- Second evil
Game.GlobalEvtLines:RemoveEvent(800)
evt.Global[800] = function()

	if Party.QBits[1564] or Party.QBits[1565] then
		Promote(const.Class.Rogue, const.Class.Assassin, {Experience = 80000})
		Message(Game.NPCText[1010])

	elseif evt.ForPlayer("All").Cmp{"Inventory", 1342} then

		Promote(const.Class.Rogue, const.Class.Assassin,
				{Experience = 80000},
				{Experience = 40000},
				15000,
				{1564, 1565})

		Party.QBits[725] = false
		Party.QBits[533] = false
		evt.Add{"Reputation", 10}

		evt.SetNPCGreeting{355, 157}
		Message(Game.NPCText[1010])

	else
		Message(Game.NPCText[1009])
	end

end

--------------------------------------------
---- Antagarich Wizard promotion
-- First
evt.Global[842] = function()
	NPCFollowers.Add(395)
end -- Golem removal part is in 7d29.lua
Game.GlobalEvtLines:RemoveEvent(843)
evt.Global[843] = function()

	local result = Promote2{
		From 	= const.Class.Sorcerer,
		To 		= const.Class.Wizard,
		PromRewards 	= {Experience = 30000},
		NonPromRewards 	= {Experience = 15000},
		Gold 	= nil,
		QBits 	= {1619, 1620},
		Awards	= nil,
		Reputation	 = -5,
		TextIdFirst	 = 1140,
		TextIdSecond = 1140,
		TextIdRefuse = 205,
		Condition	 = function() return Party.QBits[585] or Party.QBits[586] end
	}

	if result == 1 then
		evt.Subtract{"QBits", 557}
		evt.Subtract{"QBits", 731}
		evt.Subtract{"QBits", 732}
		evt.Set{"QBits", 558}
		evt.SetNPCTopic{387, 1, 844}
		evt.SetNPCGreeting{395, 199}
	end

end

-- Second good
Game.GlobalEvtLines:RemoveEvent(844)
evt.Global[844] = function()
	if CheckPromotionSide(611, 612, 1141, 1143, 1142) then
		evt.Set{"QBits", 559}
		evt.SetNPCTopic{387, 1, 845}
	end
end

Game.GlobalEvtLines:RemoveEvent(845)
evt.Global[845] = function()

	local result = Promote2{
		From 	= const.Class.Wizard,
		To 		= const.Class.ArchMage,
		PromRewards 	= {Experience = 80000},
		NonPromRewards 	= {Experience = 40000},
		Gold 	= 10000,
		QBits 	= {1621, 1622},
		Awards	= nil,
		Reputation	 = 0,
		TextIdFirst	 = 1144,
		TextIdSecond = 1144,
		TextIdRefuse = 1145,
		Condition	 = function() return evt.ForPlayer("All").Cmp{"Inventory", 1289} end
	}

	if result == 1 then
		evt.Subtract{"QBits", 559}
		evt.Subtract{"QBits", 738}
		evt.SetNPCGreeting{387, 192}
	end

end

-- Second evil
Game.GlobalEvtLines:RemoveEvent(846)
evt.Global[846] = function()

	local Allowed = Party.QBits[1619] or Party.QBits[1620]
			or evt.Cmp{"ClassIs", const.Class.Necromancer}
			or evt.Cmp{"ClassIs", const.Class.Lich}

	if Allowed then
		if Party.QBits[612] then

			Message(Game.NPCText[1146])
			evt.Set{"QBits", 560}
			evt.SetNPCTopic{388, 0, 847}

		elseif Party.QBits[611] then
			Message(Game.NPCText[1149])
		else
			Message(Game.NPCText[1148])
		end
	else
		Message(Game.NPCText[1147])
	end

end

Game.GlobalEvtLines:RemoveEvent(847)
evt.Global[847] = function()

	local NooneHave = true
	local NoLiches = true
	local JarsCount = 0

	for i, v in Party do
		if evt.ForPlayer(i).Cmp{"Inventory", 1417} then

			NooneHave = false
			JarsCount = JarsCount + 1

			if v.Class == const.Class.Wizard or v.Class == const.Class.Necromancer then
				local CurSex = v:GetSex()
				v.Class = const.Class.Lich
				evt.ForPlayer(i).Add{"Experience", 40000}
				SetLichAppearance(i, v)
				NoLiches = false
			elseif v.Class == const.Class.Lich
					and Game.Races[v.Attrs.Race].Kind ~= const.RaceKind.Undead then
				evt.ForPlayer(i).Add{"Experience", 40000}
				SetLichAppearance(i, v)
				NoLiches = false
			else
				if not Party.QBits[1624] then
					evt.ForPlayer(i).Add{"Experience", 40000}
				end
				evt.Add{"Gold", 1500}
			end

		end
	end

	for i = 1, JarsCount do
		evt.Subtract{"Inventory", 1417}
	end

	if NooneHave then

		Message(Game.NPCText[1151])

	else

		Party.QBits[1624] = true
		Party.QBits[1623] = true
		Party.QBits[560] = false
		Party.QBits[741] = false

		if NoLiches then
			Message(Game.NPCText[2698])
		else
			Message(Game.NPCText[1150])
		end

	end

end

--------------------------------------------
---- 		JADAME PROMOTIONS			----
--------------------------------------------

--------------------------------------------
---- Jadame Sorcerer promotion
Quest{
	NPC = 62, -- Lathean
	Branch = "",
	Slot = 5,
	CanShow 	= function() return evt.ForPlayer("All").Cmp{"ClassIs", const.Class.Sorcerer}
					or evt.ForPlayer("All").Cmp{"ClassIs", const.Class.Wizard}
					or evt.ForPlayer("All").Cmp{"ClassIs", const.Class.Peasant}
			end,
	CheckDone 	= function(t)	Message(t.Texts.Undone)
								return evt.Subtract{"Gold", 10000}	end,
	Done		= function() 
			Promote({const.Class.Sorcerer, const.Class.Wizard, const.Class.Peasant},
					const.Class.Necromancer,
					{Experience = 15000}, {Experience = 5000})
		end,
	After		= function()
			Promote({const.Class.Sorcerer, const.Class.Wizard, const.Class.Peasant},
					const.Class.Necromancer, {Experience = 15000})
		end,
	Texts = {	Topic 	= Game.NPCText[2699], -- "Join guild"
				Give 	= Game.NPCText[2700], -- "Well, sorcerers among you seeking for power of dark arts? Pay 10000 bill and step into our chambers."
				Undone	= Game.NPCText[2701], -- "We will not allow any rambler to sneak here. Pay and study or scram!"
				Done	= Game.NPCText[2702], -- "Perfect! Now i call all sorcerers among you necromancers, don't pretend to be good anymore."
				After	= Game.NPCText[2703]} -- "Welcome, necromancers."
	}

--------------------------------------------
---- Jadame Necromancer promotion

-- Lich
Game.GlobalEvtLines:RemoveEvent(86)
evt.Global[86] = function()
	evt.SetMessage{Str = 113}	--[[
				"Ah, you seek to achieve the ultimate in the darker arts.  The necromancer’s
				among you seek to turn themselves into Liches, the most potent of necromancers.
				Very well, I can perform this transformation for you, but you must do something
				for me...and get something for yourselves.  I require the Lost Book of Khel.
				It contains secrets of necromancy that had been hidden since Khel’s tower sank
				beneath the waves.  The Lizardmen of Dagger Wound celebrated when the sea took
				Khel and his knowledge from us.  With the volcanic upheaval in that region, I
				believe the Tower of Khel can be found.  Retrieve the book from the library and
				bring it to me!  Now, for yourselves, you will need a Lich Jar for every
				necromancer that wishes to become a Lich.  Zanthora, the Mad Necromancer keeps
				a large supply of these jars, perhaps she will sell you one!  If not you must
				take them from her!"
				]]
	Party.QBits[82] = true	-- "Find the Lost Book of Khel and return it to Vertrinus in Shadowspire."
	evt.SetNPCTopic{NPC = 61, Index = 0, Event = 1795}	-- "Vetrinus Taleshire" : "The Lost Book of Khel"
end

-- The Lost Book of Khel
evt.Global[1795] = function()
	evt.ForPlayer("All")
	if not evt.Cmp{"Inventory", Value = 611} then	-- "Lost Book of Kehl"
		evt.SetMessage{Str = 115}	--[[
				"You do not have the Lost Book of Khel!  I cannot help you, if you
				do not help me!  Return here with the Book and a Lich Jar for each
				necromancer in your party that wishes to become a Lich!"
				]]
		return
	end
	-- FIXME: use proper text
	evt.SetMessage{114}
	evt.Subtract{"Inventory", Value = 611}	-- "Lost Book of Kehl"
	Party.QBits[83] = true
	Party.QBits[82] = false	-- "Find the Lost Book of Khel and return it to Vertrinus in Shadowspire."
	evt.Add{"Experience", Value = 25000}
	evt.Add{"Awards", Value = 35}	-- "Found the Lost Book of Khel."
	Game.NPC[61].EventB = 89	-- Vetrinus Taleshire : Promotion to Lich
	Game.NPC[61].EventC = 1796	-- Vetrinus Taleshire : Promotion to Master Necromancer
	Game.NPC[61].EventD = 1797	-- Vetrinus Taleshire : Promotion to Honorary Lich
	--Game.NPC[62].EventD = 1796	-- Lathean : Promotion to Master Necromancer
	--Game.NPC[62].EventE = 1797	-- Lathean : Promotion to Honorary Lich
	evt.SetNPCTopic{NPC = 61, Index = 0, Event = 742}	-- "Vetrinus Taleshire" : "Travel with you!"
end

-- Promotion to Lich
Game.GlobalEvtLines:RemoveEvent(89)
-- FIXME: not updated on-the-fly
--[[
evt.CanShowTopic[89] = function()
	local k = math.max(Game.CurrentPlayer, 0)
	local player = Party[k]
	return table.find(Game.ClassPromotionsInv[const.Class.Lich], player.Class) and true or false
end
]]
evt.Global[89] = function()
	if not Party.QBits[83] then
		evt.SetMessage{Str = 115}	--[[
				"You do not have the Lost Book of Khel!  I cannot help you, if you
				do not help me!  Return here with the Book and a Lich Jar for each
				necromancer in your party that wishes to become a Lich!"
				]]
		return
	end
	local k = math.max(Game.CurrentPlayer, 0)
	local player = Party[k]
	evt.ForPlayer(k)
	if not (table.find(Game.ClassPromotionsInv[const.Class.Lich], player.Class)
			or player.Class == const.Class.Lich) then
		-- FIXME: show message
		return
	end
	if table.find(Game.ClassPromotionsInv[const.Class.Lich], player.Class)
			and not evt.Cmp{"Inventory", Value = 628} then	-- "Lich Jar"
		evt.SetMessage{Str = 114}	--[[
				"You have the Lost Book of Khel, however you lack the Lich Jars
				needed to complete the transformation!  Return here when you have
				one for each necromancer in your party!"
				]]
		return
	end
	if table.find(Game.ClassPromotionsInv[const.Class.Lich], player.Class) then
		evt.SetMessage{116}	--[[
			"You have brought everything needed to perform the transformation!
			So be it!  All necromancer’s in your party will be transformed into
			Liches!  May the dark energies flow through them for all eternity!
			The rest of you will gain what knowledge I can teach them as reward
			for their assistance!  Lets begin!After we have completed, good
			friend Lathean can handle any future promotions for your party."
			]]
		player.Class = const.Class.Lich
		evt.Add{"Experience", 10000}
		Party.QBits[1548] = true	-- Promoted to Lich.
		evt.Subtract{"Inventory", Value = 628}	-- "Lich Jar"
		SetLichAppearance(k, player)
		player.Attrs.PromoAwards[const.PromoAwards.JadameLich] = true
	elseif player.Class == const.Class.Lich
			and Game.Races[player.Attrs.Race].Family ~= const.RaceFamily.Undead
			and Game.Races[player.Attrs.Race].Family ~= const.RaceFamily.Ghost then
		if evt.Cmp{"Inventory", Value = 628} then	-- "Lich Jar"
			-- FIXME: show proper text
			evt.Add{"Experience", 5000}
			Party.QBits[1548] = true	-- Promoted to Lich.
			evt.Subtract{"Inventory", Value = 628}	-- "Lich Jar"
			SetLichAppearance(k, player)
			player.Attrs.PromoAwards[const.PromoAwards.JadameLich] = true
		else
			-- FIXME: use proper text
			evt.SetMessage{114}	--[[
				"You have the Lost Book of Khel, however you lack the Lich Jars
				needed to complete the transformation!  Return here when you have
				one for each necromancer in your party!"
				]]
		end
	end
end

-- Promotion to Master Necromancer
-- FIXME: not updated on-the-fly
--[[
evt.CanShowTopic[1796] = function()
	local k = math.max(Game.CurrentPlayer, 0)
	local player = Party[k]
	return table.find(Game.ClassPromotionsInv[const.Class.Lich], player.Class) and true or false
end
]]
evt.Global[1796] = function()
	if not Party.QBits[83] then
		evt.SetMessage{Str = 115}	--[[
				"You do not have the Lost Book of Khel!  I cannot help you, if you
				do not help me!  Return here with the Book and a Lich Jar for each
				necromancer in your party that wishes to become a Lich!"
				]]
		return
	end
	local k = math.max(Game.CurrentPlayer, 0)
	local player = Party[k]
	evt.ForPlayer(k)
	if not table.find(Game.ClassPromotionsInv[const.Class.Lich], player.Class) then
		-- FIXME: show message
		return
	end
	if Merge.Settings.Promotions.LichJarForMasterNecromancer == 2 then
		if not evt.Cmp{"Inventory", Value = 628} then	-- "Lich Jar"
			-- FIXME: use proper text
			evt.SetMessage{Str = 114}	--[[
				"You have the Lost Book of Khel, however you lack the Lich Jars
				needed to complete the transformation!  Return here when you have
				one for each necromancer in your party!"
				]]
			return
		else
			Party.QBits[1548] = true	-- Promoted to Lich.
			evt.Subtract{"Inventory", Value = 628}	-- "Lich Jar"
		end
	elseif Merge.Settings.Promotions.LichJarForMasterNecromancer == 1
			and not Party.QBits[1548] then
		if not evt.Cmp{"Inventory", Value = 628} then	-- "Lich Jar"
			-- FIXME: use proper text
			evt.SetMessage{Str = 114}	--[[
				"You have the Lost Book of Khel, however you lack the Lich Jars
				needed to complete the transformation!  Return here when you have
				one for each necromancer in your party!"
				]]
			return
		else
			Party.QBits[1548] = true	-- Promoted to Lich.
			evt.Subtract{"Inventory", Value = 628}	-- "Lich Jar"
		end
	end
	player.Class = const.Class.Lich
	evt.Add{"Experience", 5000}
	player.Attrs.PromoAwards[const.PromoAwards.JadameMasterNecromancer] = true
	-- TODO: update undead races
end

-- Promotion to Honorary Lich
evt.Global[1797] = function()
	if not Party.QBits[83] then
		evt.SetMessage{Str = 115}	--[[
				"You do not have the Lost Book of Khel!  I cannot help you, if you
				do not help me!  Return here with the Book and a Lich Jar for each
				necromancer in your party that wishes to become a Lich!"
				]]
		return
	end
	local k = math.max(Game.CurrentPlayer, 0)
	local player = Party[k]
	evt.ForPlayer(k)
	if not (player.Attrs.PromoAwards[const.PromoAwards.JadameLich]
			or player.Attrs.PromoAwards[const.PromoAwards.JadameMasterNecromancer]
			or player.Attrs.PromoAwards[const.PromoAwards.JadameHonoraryLich]) then
		evt.Add{"Experience", 0}
		player.Attrs.PromoAwards[const.PromoAwards.JadameHonoraryLich] = true
	end
	-- TODO: update undead races
end

-- FIXME: Lathean hasn't enough free slots
-- "Promote Necromancers"
Game.GlobalEvtLines:RemoveEvent(738)
evt.Global[738] = function()
	if not Party.QBits[83] then
		evt.SetMessage{Str = 924}	--[[
				"You have not recovered the Lost Book of Kehl!  There will be no promotions
				until you return with the book!  Speak with Vetrinus Taleshire."
				]]
		return
	end

	local NoJars = true

	for i, v in Party do
		if v.Class == const.Class.Necromancer or v.Class == const.Class.Wizard then
			if evt[i].Cmp{"Inventory", 628} then
				evt[i].Subtract{"Inventory", 628}
				evt[i].Add{"Experience", 0}
				v.Class = const.Class.Lich
				NoJars = false
				SetLichAppearance(i, v)
				player.Attrs.PromoAwards[const.PromoAwards.JadameLich] = true
			end
		elseif v.Class == const.Class.Lich
				and Game.Races[v.Attrs.Race].Kind ~= const.RaceKind.Undead
				and evt[i].Cmp{"Inventory", Value = 628} then	-- "Lich Jar"
			evt[i].Subtract{"Inventory", 628}
			evt[i].Add{"Experience", 0}
			NoJars = false
			SetLichAppearance(i, v)
			player.Attrs.PromoAwards[const.PromoAwards.JadameLich] = true
		end
	end

	if NoJars then
		Message(Game.NPCText[114])
	else
		Message(Game.NPCText[925])
	end

end

--[[
evt.Global[738] = function()
	if not Party.QBits[83] then
		evt.SetMessage{Str = 924}]]	--[[
				"You have not recovered the Lost Book of Kehl!  There will be no promotions
				until you return with the book!  Speak with Vetrinus Taleshire."
				]]
--[[		return
	end
	local k = math.max(Game.CurrentPlayer, 0)
	local player = Party[k]
	evt.ForPlayer(k)
	if not (table.find(Game.ClassPromotionsInv[const.Class.Lich], player.Class)
			or player.Class == const.Class.Lich) then
		-- FIXME: show message
		return
	end
	if table.find(Game.ClassPromotionsInv[const.Class.Lich], player.Class)
			and not evt.Cmp{"Inventory", Value = 628} then	-- "Lich Jar"
		evt.SetMessage{Str = 114}]]	--[[
				"You have the Lost Book of Khel, however you lack the Lich Jars
				needed to complete the transformation!  Return here when you have
				one for each necromancer in your party!"
				]]
--[[		return
	end
	if table.find(Game.ClassPromotionsInv[const.Class.Lich], player.Class) then
		evt.SetMessage{925}]]	--[[
			"Ah, you return seeking promotion for others in your party?
			I have not forgotten your help in recovering the Lost Book of Kehl!
			All Necromancers in your party will be promoted to Lich.  Be sure
			each Necromancer has a Lich Jar in his possession."
			]]
--[[		player.Class = const.Class.Lich
		evt.Add{"Experience", 10000}
		Party.QBits[1548] = true	-- Promoted to Lich.
		evt.Subtract{"Inventory", Value = 628}	-- "Lich Jar"
		SetLichAppearance(k, player)
		player.Attrs.PromoAwards[const.PromoAwards.JadameLich] = true
	elseif player.Class == const.Class.Lich
			and Game.Races[player.Attrs.Race].Family ~= const.RaceFamily.Undead
			and Game.Races[player.Attrs.Race].Family ~= const.RaceFamily.Ghost then
		if evt.Cmp{"Inventory", Value = 628} then	-- "Lich Jar"
			-- FIXME: show proper text
			evt.Add{"Experience", 5000}
			Party.QBits[1548] = true	-- Promoted to Lich.
			evt.Subtract{"Inventory", Value = 628}	-- "Lich Jar"
			SetLichAppearance(k, player)
			player.Attrs.PromoAwards[const.PromoAwards.JadameLich] = true
		else
			-- FIXME: use proper text
			evt.SetMessage{114}]]	--[[
				"You have the Lost Book of Khel, however you lack the Lich Jars
				needed to complete the transformation!  Return here when you have
				one for each necromancer in your party!"
				]]
--[[		end
	end
end
]]

--------------------------------------------
---- Jadame Cleric/Priest promotion

-- "Quest"
Game.GlobalEvtLines:RemoveEvent(81)
--evt.CanShowTopic[81] = function()
--	return evt.Cmp{"Inventory", Value = 626}	-- "Prophecies of the Sun"
--end

evt.Global[81] = function()
	evt.ForPlayer("All")
	if not evt.Cmp{"Inventory", Value = 626} then	-- "Prophecies of the Sun"
		evt.SetMessage{106}	-- "Have you found this Lair of the Feathered Serpent and the Prophecies of the Sun?  Do not waste my time!  The world is ending and you waste time with useless conversation!  Return to me when you have found the Prophecies and have taken them to the Temple of the Sun."
		return
	end
	evt.SetMessage{107}	-- "You have found the lost Prophecies of the Sun?  May the Light forever shine upon you and may the Prophet guide your steps.  With these we may be able to find the answer to what has befallen Jadame! "
	evt.Add{"Experience", 25000}
	evt.Add{"Awards", 31}	-- "Found the lost Prophecies of the Sun and returned them to the Temple of the Sun."
	evt.Subtract{"Inventory", 626}	-- "Prophecies of the Sun"
	Party.QBits[79] = true
	Party.QBits[78] = false	-- "Find the Prophecies of the Sun in the Abandoned Temple  and take them to Stephen."
	Game.NPC[59].EventB = 1798	-- Stephen : Promotion to Honorary Priest of the Light
	Game.NPC[59].EventC = 0	-- remove "Quest" topic
	Game.NPC[59].EventD = 0	-- remove "Prophecies of the Sun" topic
	Game.NPC[59].EventE = 0	-- remove "Clues" topic
	evt.SetNPCTopic{NPC = 59, Index = 0, Event = 737}	-- "Stephen" : "Promote Clerics"
end

-- FIXME: change NPCTopic
-- "Promote Clerics"
Game.GlobalEvtLines:RemoveEvent(737)
evt.Global[737] = function()
	if not Party.QBits[79] then
		evt.SetMessage{922}	-- "You cannot be promoted to Priest of the Sun until you have recovered the Prophecies of the Sun!"
		return
	end
	evt.SetMessage{923}	-- "You are always welcome here!  Of course I will promote any Clerics that travel with you to Priest of the Sun!  "
	local k = math.max(Game.CurrentPlayer, 0)
	local player = Party[k]
	evt.ForPlayer(k)
	-- FIXME: use Game.ClassPromotionsInv
	if table.find({const.Class.Cleric, const.Class.Priest, const.Class.ClericLight}, player.Class) then
		player.Class = const.Class.PriestLight
		evt.Add{"Experience", 10000}
		player.Attrs.PromoAwards[const.PromoAwards.JadamePriestLight] = true
		Party.QBits[1546] = true	-- Promoted to Cleric of the Sun. // Actually Priest of the Light
		if Merge.Settings.Races.MaxMaturity > 0
				and player.Attrs.Race == const.Race.Human then
			player.Attrs.Race = const.Race.MatureHuman
		end
	end
end

-- Promotion to Honorary Priest of the Light
evt.Global[1798] = function()
	-- FIXME: use proper text
	if not Party.QBits[79] then
		evt.SetMessage{922}	-- "You cannot be promoted to Priest of the Sun until you have recovered the Prophecies of the Sun!"
		return
	end
	-- FIXME: use proper text
	evt.SetMessage{923}	-- "You are always welcome here!  Of course I will promote any Clerics that travel with you to Priest of the Sun!  "
	local k = math.max(Game.CurrentPlayer, 0)
	local player = Party[k]
	evt.ForPlayer(k)
	if not (player.Attrs.PromoAwards[const.PromoAwards.JadamePriestLight]
			or player.Attrs.PromoAwards[const.PromoAwards.JadameHonoraryPriestLight]) then
		evt.Add{"Experience", 0}
		player.Attrs.PromoAwards[const.PromoAwards.JadameHonoraryPriestLight] = true
	end
	if Merge.Settings.Races.MaxMaturity > 0
			and player.Attrs.Race == const.Race.Human then
		player.Attrs.Race = const.Race.MatureHuman
	end
end

--------------------------------------------
---- Jadame Dark Elf promotion

-- "Thank you!"
Game.GlobalEvtLines:RemoveEvent(24)
evt.Global[24] = function()
	evt.SetMessage{26}	--[[
		"Thank you for your assistance with the Basilisk Curse.  Usually I am
		prepared to handle the vile lizards, but this time there were just too
		many of them.The Temple of the Sun asked me to check on a few pilgrims
		that were looking for the Druid Circle of Stone in this area.  When I
		found the first statue, I realized what had happened to the pilgrims.
		I myself did not know of the increase in the number of Basilisks in this
		area.  They seem to be agitated by something.  I was going to investigate
		the Druid Circle of Stone when the Basilisks attacked me."
		]]
	-- See out07.lua, evt.Map[132]
	--[[
	evt.ForPlayer("All")
	evt.Add{"Experience", 25000}
	evt.Add{"Awards", 20}	-- "Rescued Cauri Blackthorne."
	Party.QBits[39] = false	-- "Find Cauri Blackthorne then return to Dantillion in Murmurwoods with information of her location."
	Party.QBits[40] = true	-- Found and Rescued Cauri Blackthorne
	Party.QBits[430] = true	-- Roster Character In Party 31
	Game.NPC[42].EventB = 25	-- Cauri Blackthorne : Promotion to Patriarch
	Game.NPC[42].EventC = 1799	-- Cauri Blackthorne : Promotion to Honorary Patriarch
	Game.NPC[39].EventD = 1799	-- Relburn Jeebes : Promotion to Honorary Patriarch
	]]
	evt.SetNPCTopic{NPC = 42, Index = 0, Event = 38}	-- "Cauri Blackthorne" : "Thanks for your help!"
end

-- FIXME: [Street] NPC screen won't allow to switch between party members
-- FIXME: Change NPCTopic
-- "Patriarch"
Game.GlobalEvtLines:RemoveEvent(25)
evt.Global[25] = function()
	if not Party.QBits[40] then
		return
	end
	local k = math.max(Game.CurrentPlayer, 0)
	local player = Party[k]
	evt.ForPlayer(k)
	if not table.find(Game.ClassPromotionsInv[const.Class.Patriarch], player.Class) then
		-- FIXME: show message
		return
	end
	-- FIXME: show next message once, use other text when QBit 1537 is set
	evt.SetMessage{27}	--[[
		"::Cauri looks over the Dark Elf(s) in your party::To rescue me, you must have
		researched my path, and investigated the places I had been.  This demonstrates
		the intelligence needed to succeed in dealing with the world and business.To
		get to where I was attacked, you must have the skills needed to fight the
		Basilisks and other threats, demonstrating your prowess as a warrior.  Skill
		in battle is needed when proper negotiations break down.To ask me for promotion
		demonstrates desire, and without desire success will always escape your grasp.You
		have all of the traits necessary to hold the title of Patriarch.  I will notify
		to Council upon my return.  It would be my pleasure to travel with you again.
		You can find me in the Inn in Ravenshore."
		]]
	player.Class = const.Class.Patriarch
	evt.Add{"Experience", 10000}
	player.Attrs.PromoAwards[const.PromoAwards.JadamePatriarch] = true
	Party.QBits[1537] = true	-- Promoted to Elf Patriarch.
	if Merge.Settings.Races.MaxMaturity > 0
			and player.Attrs.Race == const.Race.DarkElf then
		player.Attrs.Race = const.Race.MatureDarkElf
	end
end

-- Promotion to Honorary Patriarch
evt.Global[1799] = function()
	if not Party.QBits[40] then	-- Found and Rescued Cauri Blackthorne
		return
	end
	local k = math.max(Game.CurrentPlayer, 0)
	local player = Party[k]
	evt.ForPlayer(k)
	if not (player.Attrs.PromoAwards[const.PromoAwards.JadamePatriarch]
			or player.Attrs.PromoAwards[const.PromoAwards.JadameHonoraryPatriarch]) then
		evt.Add{"Experience", 0}
		player.Attrs.PromoAwards[const.PromoAwards.JadameHonoraryPatriarch] = true
	end
	if Merge.Settings.Races.MaxMaturity > 0
			and player.Attrs.Race == const.Race.DarkElf then
		player.Attrs.Race = const.Race.MatureDarkElf
	end
end

-- "Promote Dark Elves"
Game.GlobalEvtLines:RemoveEvent(733)
evt.CanShowTopic[733] = function()
	return Party.QBits[39] or Party.QBits[40]
end

evt.Global[733] = function()
	if not Party.QBits[40] then	-- Found and Rescued Cauri Blackthorne
		evt.SetMessage{914}	--[[
			"You cannot be promoted until we know where Cauri Blackthorne is!
			Locate her, and she will promote you.  After she is found and has
			retuned to the Merchant Guild, I will be glad to promote any Dark
			Elves in your party to Patriarch."
			]]
		return
	end
	local k = math.max(Game.CurrentPlayer, 0)
	local player = Party[k]
	evt.ForPlayer(k)
	if not table.find(Game.ClassPromotionsInv[const.Class.Patriarch], player.Class) then
		-- FIXME: show message
		return
	end
	evt.SetMessage{915}	--[[
		"Cauri told me of how you helped her with the curse of the Basilisk.
		She has instructed me to promote any Dark Elves that travel with you
		to Patriarch."
		]]
	player.Class = const.Class.Patriarch
	-- Note: no experience award in MM8
	evt.Add{"Experience", 10000}
	player.Attrs.PromoAwards[const.PromoAwards.JadamePatriarch] = true
	Party.QBits[1537] = true	-- Promoted to Elf Patriarch.
	if Merge.Settings.Races.MaxMaturity > 0
			and player.Attrs.Race == const.Race.DarkElf then
		player.Attrs.Race = const.Race.MatureDarkElf
	end
end

--------------------------------------------
---- Jadame Dragon promotion

-- "Sword of the Slayer"
Game.GlobalEvtLines:RemoveEvent(62)
evt.CanShowTopic[62] = function()
	return Party.QBits[75]	-- Killed all Dragon Slayers in southwest encampment in Area 5
end

evt.Global[62] = function()
	evt.ForPlayer("All")
	if not evt.Cmp{"Inventory", 540} then	-- "Sword of Whistlebone"
		evt.SetMessage{81}	--[[
			"You have killed the Dragon Slayers, but where is the Sword
			of Whistlebone?  Return to me when you have it!"
			]]
		return
	end
	if Party.QBits[22]	-- Allied with Dragons. Return Dragon Egg to Dragons done.
			or not Party.QBits[21] then	-- Allied with Charles Quioxte's Dragon Hunters. Return Dragon Egg to Quixote done.
		evt.SetMessage{79}	--[[
			"You return to me with the sword of the Slayer, Whistlebone!
			You are indeed worthy of my notice!  The Dragons in your group are
			promoted to Great Wyrm!  I will teach the others of your group what
			skills I can as a reward for their assistance!"
			]]
	else
		evt.SetMessage{80}	--[[
			"You return to me with the sword of the Slayer, Whistlebone!
			Is there no end to the treachery that you will commit? Is there
			no one that you owe allegiance to?  I will promote those Dragons
			who travel with you to Great Wyrm, however they will never fly
			underneath me!  There rest of your traitorous group will be
			instructed in those skills which can be taught to them!  Go now!
			Never show your face here again, unless you want it eaten!"
			]]
	end
	evt.Add{"Experience", 25000}
	evt.Subtract{"Inventory", 540}	-- "Sword of Whistlebone"

	Party.QBits[86] = true
	Party.QBits[74] = false	-- "Kill all Dragon Slayers and return the Sword of Whistlebone the Slayer to Deftclaw Redreaver in Garrote Gorge."
	Game.NPC[17].EventD = 1800	-- Deftclaw Redreaver : Promotion to Honorary Great Wyrm
	Game.NPC[53].EventC = 736	-- Deftclaw Redreaver : Promote Dragons
	Game.NPC[53].EventD = 1800	-- Deftclaw Redreaver : Promotion to Honorary Great Wyrm
	evt.SetNPCTopic{NPC = 17, Index = 2, Event = 736}	-- "Deftclaw Redreaver" : "Promote Dragons"
	evt.SetNPCTopic{NPC = 53, Index = 2, Event = 736}	-- "Deftclaw Redreaver" : "Promote Dragons"
end

-- "Promote Dragons"
Game.GlobalEvtLines:RemoveEvent(736)
evt.Global[736] = function()
	if not Party.QBits[86] then
		evt.SetMessage{920}	--[[
			"You have not proven yourself worthy!  Until you strike a blow against
			the Dragon Hunters, none of you will be promoted!"
			]]
		return
	end
	local k = math.max(Game.CurrentPlayer, 0)
	local player = Party[k]
	evt.ForPlayer(k)
	if not table.find(Game.ClassPromotionsInv[const.Class.GreatWyrm], player.Class) then
		-- FIXME: show message
		return
	end
	evt.SetMessage{921}	--[[
		"You have proven yourself and I will promote any Dragons that travel
		with you to Great Wyrm."
		]]
	player.Class = const.Class.GreatWyrm
	-- Note: in MM8 - experience award is only on bringing Sword of Whistlebone
	evt.Add{"Experience", Value = 10000}
	player.Attrs.PromoAwards[const.PromoAwards.JadameGreatWyrm] = true
	if Merge.Settings.Races.MaxMaturity > 0
			and player.Attrs.Race == const.Race.Dragon then
		player.Attrs.Race = const.Race.MatureDragon
	end
	Party.QBits[1543] = true	-- Promoted to Great Wyrm.
end

-- Promotion to Honorary Great Wyrm
evt.Global[1800] = function()
	if not Party.QBits[86] then
		return
	end
	local k = math.max(Game.CurrentPlayer, 0)
	local player = Party[k]
	evt.ForPlayer(k)
	if not (player.Attrs.PromoAwards[const.PromoAwards.JadameGreatWyrm]
			or player.Attrs.PromoAwards[const.PromoAwards.JadameHonoraryGreatWyrm]) then
		evt.Add{"Experience", 0}
		player.Attrs.PromoAwards[const.PromoAwards.JadameHonoraryGreatWyrm] = true
	end
	if Merge.Settings.Races.MaxMaturity > 0
			and player.Attrs.Race == const.Race.Dragon then
		player.Attrs.Race = const.Race.MatureDragon
	end
end

--------------------------------------------
---- Jadame Knight/Cavalier promotion

-- "Promotion to Champion"
Game.GlobalEvtLines:RemoveEvent(58)
evt.CanShowTopic[58] = function()
	return Party.QBits[73]	-- Received Cure for Blazen Stormlance
end

evt.Global[58] = function()
	evt.ForPlayer("All")
	if not evt.Cmp{"Inventory", 539} then	-- "Ebonest"
		evt.SetMessage{85}	--[[
			"You have found Blazen Stormlance? But where is Ebonest?
			Return to me when you have the spear and you will be promoted!"
			]]
		return
	end
	if Party.QBits[22] then	-- Allied with Dragons. Return Dragon Egg to Dragons done.
		evt.SetMessage{71}	--[[
			"What is this?  You ally with my mortal enemies and then seek
			to do me a favor?I wonder what the Dragons think of this. But so
			be it.  I am in your debt for returning Ebonest to me.  I will
			promote any Knights in your party to Champion, however they will
			never be accepted in my service.  The rest I will teach what I can.
			I do not wish to see you again!"
			]]
	else
		evt.SetMessage{70}	--[[
			"You found Blazen Stormlance?  What about MY spear Ebonest?  You
			have that as well?FANTASTIC!I thank you for this and find myself
			in your debt!  I will promote all knights in your party to
			Champion and teach what skills I can to the rest of your party. "
			]]
	end
	evt.Add{"Experience", 35000}
	evt.Subtract{"Inventory", 539}	-- "Ebonest"
	Party.QBits[71] = true
	Party.QBits[70] = false	-- "Find Blazen Stormlance and recover the spear Ebonest. Return to Leane Stormlance in Garrote Gorge and deliver Ebonest to Charles Quixote."
	Game.NPC[15].EventD = 1801	-- Sir Charles Quixote : Promotion to Honorary Champion
	Game.NPC[52].EventC = 735	-- Sir Charles Quixote : Promote Knights
	Game.NPC[52].EventD = 1801	-- Sir Charles Quixote : Promotion to Honorary Champion
	evt.SetNPCTopic{NPC = 15, Index = 2, Event = 735}	-- "Sir Charles Quixote" : "Promote Knights"
	evt.SetNPCTopic{NPC = 52, Index = 2, Event = 735}	-- "Sir Charles Quixote" : "Promote Knights"
end

-- "Promote Knights"
Game.GlobalEvtLines:RemoveEvent(735)
evt.Global[735] = function()
	if not Party.QBits[71] then
		evt.SetMessage{918}	--[[
			"You cannot be promoted to Champion until you have proven yourself worthy!  "
			]]
		return
	end
	local k = math.max(Game.CurrentPlayer, 0)
	local player = Party[k]
	evt.ForPlayer(k)
	if not table.find({const.Class.Knight, const.Class.Cavalier}, player.Class) then
		-- FIXME: show message
		return
	end
	evt.SetMessage{919}	--[[
		"Thanks for you help recovering the spear Ebonest!  I can promote
		any Knights that travel with you to Champion."
		]]
	player.Class = const.Class.Champion
	-- Note: in MM8 - experience award is only on bringing Ebonest
	evt.Add{"Experience", Value = 15000}
	player.Attrs.PromoAwards[const.PromoAwards.JadameChampion] = true
	if Merge.Settings.Races.MaxMaturity > 0
			and player.Attrs.Race == const.Race.Human then
		player.Attrs.Race = const.Race.MatureHuman
	end
	Party.QBits[1540] = true	-- Promoted to Champion.
end

-- Promotion to Honorary Champion
evt.Global[1801] = function()
	if not Party.QBits[71] then
		return
	end
	local k = math.max(Game.CurrentPlayer, 0)
	local player = Party[k]
	evt.ForPlayer(k)
	if not (player.Attrs.PromoAwards[const.PromoAwards.JadameChampion]
			or player.Attrs.PromoAwards[const.PromoAwards.JadameHonoraryChampion]) then
		evt.Add{"Experience", 0}
		player.Attrs.PromoAwards[const.PromoAwards.JadameHonoraryChampion] = true
	end
	if Merge.Settings.Races.MaxMaturity > 0
			and player.Attrs.Race == const.Race.Human then
		player.Attrs.Race = const.Race.MatureHuman
	end
end

--------------------------------------------
---- Jadame Minotaur promotion

-- "Quest"
Game.GlobalEvtLines:RemoveEvent(71)
-- Don't hide Quest topic
--[[
evt.CanShowTopic[71] = function()
	return evt.Cmp{"Inventory", 732}	-- "Certificate of Authentication"
end
]]

evt.Global[71] = function()
	evt.ForPlayer("All")
	if not evt.Cmp{"Inventory", 541} then	-- "Axe of Balthazar"
		evt.SetMessage{98}	--[[
			"Where is Balthazar's Axe?  You waste my time!
			Find the axe, find Dadeross and return to me!"
			]]
		return
	end
	if not evt.Cmp{"Inventory", 732} then	-- "Certificate of Authentication"
		evt.SetMessage{94}	--[[
			"You have found the Axe of Balthazar!  Have you presented it
			to Dadeross?  Without his authentication, we can not proceed
			with the Rite’s of Purity!  Find him and return to us once
			you have presented him with the axe!"
			]]
		return
	end
	evt.SetMessage{95}	--[[
		"You have found the Axe of Balthazar!  Have you presented it to
		Dadeross? Ah, you have authentication from Dadeross!  The Rite’s
		of Purity will begin immediately! You proven yourselves worthy, and
		our now members of our herd!  The Minotaurs who travel with you are
		promoted to Minotaur Lord.  The others in your group will be taught
		what skills we have that maybe useful to them."
		]]
	evt.Add{"Experience", 25000}
	evt.Add{"Awards", 29}	-- "Recovered Axe of Balthazar."
	Party.QBits[76] = false	-- "Find the Axe of Balthazar, in the Dark Dwarf Mines.  Have the Axe authenticated by Dadeross.  Return the axe to Tessalar, heir to the leadership of the Minotaur Herd."
	Party.QBits[77] = true	-- Found the Axe of Balthazar.
	evt.Subtract{"Inventory", 541}	-- "Axe of Balthazar"
	evt.Subtract{"Inventory", 732}	-- "Certificate of Authentication"
	Game.NPC[58].EventB = 1802	-- Tessalar : Promotion to Honorary Minotaur Lord
	Game.NPC[58].EventC = 70	-- Tessalar : Dark Dwarves
	evt.SetNPCTopic{NPC = 58, Index = 0, Event = 740}         -- "Tessalar" : "Promote Minotuars"
end

-- "Hurry!"
Game.GlobalEvtLines:RemoveEvent(75)
evt.Global[75] = function()
	if Party.QBits[77] then	-- Found the Axe of Balthazar.
		evt.SetMessage{102}	--[[
			"Ah, you returned Balthazar's Axe with my certificate to
			Tessalar!  The Rites of Purity have begun.  Soon Lord Masul
			will wield his father's axe as his own!  Greatness will
			return to my herd!"
			]]
	else
		evt.SetMessage{101}	--[[
			"You have found Balthazar's Axe and I have authenticated it!
			Hurry back to Tessalar so that the Rites of Purity may begin!"
			]]
	end
end

-- "Promote Minotuars"
Game.GlobalEvtLines:RemoveEvent(740)
evt.Global[740] = function()
	if not Party.QBits[77] then	-- Found the Axe of Balthazar.
		evt.SetMessage{928}	--[[
			"You have not found the Axe of Balthazar!
			You are not worthy of promotion!"
			]]
		return
	end
	local k = math.max(Game.CurrentPlayer, 0)
	local player = Party[k]
	evt.ForPlayer(k)
	if not table.find(Game.ClassPromotionsInv[const.Class.MinotaurLord], player.Class) then
		-- FIXME: show message
		return
	end
	evt.SetMessage{929}	--[[
		"The Herd of Masul is in debt to you.  Any Minotaurs
		in your party are promoted to Minotaur Lord!"
		]]
	player.Class = const.Class.MinotaurLord
	-- Note: in MM8 - experience award is only on bringing Axe of Balthazar
	evt.Add{"Experience", Value = 10000}
	player.Attrs.PromoAwards[const.PromoAwards.JadameMinotaurLord] = true
	if Merge.Settings.Races.MaxMaturity > 0
			and player.Attrs.Race == const.Race.Minotaur then
		player.Attrs.Race = const.Race.MatureMinotaur
	end
	Party.QBits[1545] = true	-- Promoted to Minotaur Lord.
end

-- Promotion to Honorary Minotaur Lord
evt.Global[1802] = function()
	if not Party.QBits[77] then	-- Found the Axe of Balthazar.
		return
	end
	local k = math.max(Game.CurrentPlayer, 0)
	local player = Party[k]
	evt.ForPlayer(k)
	if not (player.Attrs.PromoAwards[const.PromoAwards.JadameMinotaurLord]
			or player.Attrs.PromoAwards[const.PromoAwards.JadameHonoraryMinotaurLord]) then
		evt.Add{"Experience", 0}
		player.Attrs.PromoAwards[const.PromoAwards.JadameHonoraryMinotaurLord] = true
	end
	if Merge.Settings.Races.MaxMaturity > 0
			and player.Attrs.Race == const.Race.Minotaur then
		player.Attrs.Race = const.Race.MatureMinotaur
	end
end

--------------------------------------------
---- Jadame Berserker/Troll promotion

-- "Ancient Home Found!"
Game.GlobalEvtLines:RemoveEvent(36)
evt.CanShowTopic[36] = function()
	return Party.QBits[69]	-- Ancient Troll Homeland Found
end

evt.Global[36] = function()
	if not Party.QBits[69] then
		evt.SetMessage{49}	--[[
			"Have you found the Ancient Home for our Clan?
			Without its location, my people will surely perish!"
			]]
		return
	end
	evt.SetMessage{45}	--[[
		"You have found our Ancient Home?  Its located in the western area
		of the Murmurwoods?  This is wonderful news.  Perhaps there is still
		time to move my people.  Unfortunately the Elemental threat must be
		dealt with first, or no people will be safe! All Trolls among you
		have been promoted to War Troll, and their names will be forever
		remembered in our songs.  I will teach the rest of you what skills
		I can, perhaps it will be enough to help you save all of Jadame."
		]]
	evt.ForPlayer("All")
	evt.Add{"Experience", Value = 25000}
	Party.QBits[87] = true
	Party.QBits[68] = false	-- "Find the Ancient Troll Homeland and return to Volog Sandwind in the Ironsand Desert."
	Game.NPC[43].EventB = 1803	-- Volog Sandwind : Promotion to Warmonger
	Game.NPC[43].EventC = 1804	-- Volog Sandwind : Promotion to Honorary Warmonger
	Game.NPC[67].EventB = 1804	-- Hobb Sandwind : Promotion to Honorary Warmonger
	evt.SetNPCTopic{NPC = 43, Index = 0, Event = 612}	-- "Volog Sandwind" : "Roster Join Event"
end

-- Promotion to Warmonger
evt.Global[1803] = function()
	if not Party.QBits[87] then
		evt.SetMessage{49}	--[[
			"Have you found the Ancient Home for our Clan?
			Without its location, my people will surely perish!"
			]]
		return
	end
	local k = math.max(Game.CurrentPlayer, 0)
	local player = Party[k]
	evt.ForPlayer(k)
	if not table.find(Game.ClassPromotionsInv[const.Class.Warmonger], player.Class) then
		-- FIXME: show message
		return
	end
	-- FIXME: show message
	player.Class = const.Class.Warmonger
	-- Note: in MM8 - experience award is only on first time
	evt.Add{"Experience", Value = 10000}
	player.Attrs.PromoAwards[const.PromoAwards.JadameWarmonger] = true
	if Merge.Settings.Races.MaxMaturity > 0
			and player.Attrs.Race == const.Race.Troll then
		player.Attrs.Race = const.Race.MatureTroll
	end
	Party.QBits[1538] = true	-- Promoted to War Troll.
end

-- Promotion to Honorary Warmonger
evt.Global[1804] = function()
	if not Party.QBits[87] then
		evt.SetMessage{49}	--[[
			"Have you found the Ancient Home for our Clan?
			Without its location, my people will surely perish!"
			]]
		return
	end
	local k = math.max(Game.CurrentPlayer, 0)
	local player = Party[k]
	evt.ForPlayer(k)
	if not (player.Attrs.PromoAwards[const.PromoAwards.JadameWarmonger]
			or player.Attrs.PromoAwards[const.PromoAwards.JadameHonoraryWarmonger]) then
		evt.Add{"Experience", 0}
		player.Attrs.PromoAwards[const.PromoAwards.JadameHonoraryWarmonger] = true
	end
	if Merge.Settings.Races.MaxMaturity > 0
			and player.Attrs.Race == const.Race.Troll then
		player.Attrs.Race = const.Race.MatureTroll
	end
end

-- "Promote Trolls"
Game.GlobalEvtLines:RemoveEvent(734)
evt.Global[734] = function()
	if not Party.QBits[87] then
		evt.SetMessage{916}	--[[
			"You cannot be promoted until the Ancient Troll Home has been
			found!  Return here when you have found it and talk with Volog."
			]]
		return
	end
	local k = math.max(Game.CurrentPlayer, 0)
	local player = Party[k]
	evt.ForPlayer(k)
	if not table.find(Game.ClassPromotionsInv[const.Class.Warmonger], player.Class) then
		-- FIXME: show message
		return
	end
	evt.SetMessage{917}	--[[
		"Before Volog left, he instructed me to promote any Trolls that travel
		with you to War Troll.  The village of Rust will be forever thankful to you!"
		]]
	player.Class = const.Class.Warmonger
	-- Note: in MM8 - experience award is only on first time
	evt.Add{"Experience", Value = 10000}
	player.Attrs.PromoAwards[const.PromoAwards.JadameWarmonger] = true
	if Merge.Settings.Races.MaxMaturity > 0
			and player.Attrs.Race == const.Race.Troll then
		player.Attrs.Race = const.Race.MatureTroll
	end
	Party.QBits[1538] = true	-- Promoted to War Troll.
end

--------------------------------------------
---- Jadame Vampire promotion

-- "Return of Korbu"
Game.GlobalEvtLines:RemoveEvent(90)
evt.CanShowTopic[90] = function()
	return Party.QBits[80]	-- "Find the Sarcophagus of Korbu and Korbu's Remains and return them to Lathean in Shadowspire."
end

evt.Global[90] = function()
	evt.ForPlayer("All")
	if not evt.Cmp{"Inventory", 627} then	-- "Remains of Korbu"
		if evt.Cmp{"Inventory", 612} then	-- "Sarcophagus of Korbu"
			evt.SetMessage{112}	--[[
				"You return to us with the Sarcophagus of Korbu, but
				where are his remains? We cannot complete the act of
				reanimation without them!  Return to us when you have
				both the Sarcophagus and his remains!"
				]]
		else
			evt.SetMessage{151}	--[[
				"We need to consult Korbu!  You have agreed to bring us
				his remains and his Sarcophagus!  Do not bother us until
				you have these items!"
				]]
		end
		return
	end
	if not evt.Cmp{"Inventory", 612} then	-- "Sarcophagus of Korbu"
		evt.SetMessage{111}	--[[
			"You return to us with the remains of Korbu, but where is his
			Sarcophagus? We cannot complete the act of reanimation without it!
			Return to us when you have both the remains and the Sarcophagus!"
			]]
		return
	end
	evt.SetMessage{117}	--[[
		"You have brought us the Sarcophagus of Korbu and his sacred remains.
		We will attempt to reanimate Korbu and seek his wisdom in these
		troubled times!  The vampires among you will be transformed into
		Nosferatu, and the others will be taught what skills we can teach
		them as reward for your service."
		]]
	evt.Add{"Experience", Value = 25000}
	evt.Add{"Awards", Value = 33}         -- "Found the Sarcophagus and Remains of Korbu."
	Party.QBits[88] = true
	Party.QBits[80] = false	-- "Find the Sarcophagus of Korbu and Korbu's Remains and return them to Lathean in Shadowspire."
	evt.Subtract{"Inventory", 627}	-- "Remains of Korbu"
	evt.Subtract{"Inventory", 612}	-- "Sarcophagus of Korbu"
	Game.NPC[62].EventB = 1805	-- Lathean : Promotion to Honorary Nosferatu
	evt.SetNPCTopic{NPC = 62, Index = 0, Event = 739}	-- "Lathean" : "Promote Vampires"
end

-- "Promote Vampires"
Game.GlobalEvtLines:RemoveEvent(739)
evt.Global[739] = function()
	if not Party.QBits[88] then
		evt.SetMessage{926}	--[[
			"You have not found Korbu and until you have I refuse to promote any of you!  Begone!"
			]]
		return
	end
	local k = math.max(Game.CurrentPlayer, 0)
	local player = Party[k]
	evt.ForPlayer(k)
	if not table.find(Game.ClassPromotionsInv[const.Class.Nosferatu], player.Class) then
		-- FIXME: show message
		return
	end
	evt.SetMessage{927}	--[[
		"Any Vampires among you will be promoted to Nosferatu!  I remember
		those who helped in the recovery of the Remains of Korbu."
		]]
	player.Class = const.Class.Nosferatu
	-- Note: in MM8 - experience award is only on bringing Korbu
	evt.Add{"Experience", Value = 10000}
	player.Attrs.PromoAwards[const.PromoAwards.JadameNosferatu] = true
	-- TODO: update vampire races
	Party.QBits[1547] = true	-- Promoted to Nosferatu.
end

-- Promotion to Honorary Nosferatu
evt.Global[1805] = function()
	if not Party.QBits[88] then
		evt.SetMessage{926}	--[[
			"You have not found Korbu and until you have I refuse to promote any of you!  Begone!"
			]]
		return
	end
	local k = math.max(Game.CurrentPlayer, 0)
	local player = Party[k]
	evt.ForPlayer(k)
	if not (player.Attrs.PromoAwards[const.PromoAwards.JadameNosferatu]
			or player.Attrs.PromoAwards[const.PromoAwards.JadameHonoraryNosferatu]) then
		evt.Add{"Experience", 0}
		player.Attrs.PromoAwards[const.PromoAwards.JadameHonoraryNosferatu] = true
	end
	-- TODO: update vampire races
end

--------------------------------------------
---- 		PEASANT PROMOTIONS			----
--------------------------------------------

-- SkillId = Class
-- Teachers will also promote peasants to class assigned to skill.
local TeacherPromoters = {

	[0] = const.Class.Monk,	-- Staff = Monk
	[1] = const.Class.Knight,	-- Sword = Knight
	[2] = const.Class.Thief,	-- Dagger = Thief
	--[3] = const.Class.Ranger	-- Axe = Ranger
	[4] = const.Class.Knight,	-- Spear = Knight
	[5] = const.Class.Archer,	-- Bow = Archer
	[6] = const.Class.Cleric,	-- Mace = Cleric
	[7] = nil,
	[8] = const.Class.Paladin,	-- Shield = Paladin
	[9] = const.Class.Thief,	-- Leather = Thief
	[10] = const.Class.Archer,	-- Chain = Archer
	[11] = const.Class.Paladin,	-- Plate = Paladin
	[12] = const.Class.Sorcerer, 	-- Fire = Sorcerer
	[13] = const.Class.Sorcerer,	-- Air = Sorcerer
	[14] = const.Class.Druid,	-- Water = Druid
	[15] = const.Class.Druid,	-- Earth = Druid
	[16] = const.Class.Paladin,	-- Spirit = Paladin
	[17] = const.Class.Druid,	-- Mind = Druid
	[18] = const.Class.Cleric,	-- Body = Cleric
	[19] = const.Class.Cleric,	-- Light = Cleric
	[20] = const.Class.Sorcerer,	-- Dark = Sorcerer
	[21] = const.Class.DarkElf,	-- Dark elf
	[22] = const.Class.Vampire,	-- Vampire
	[23] = nil,
	[24] = const.Class.Thief,	-- ItemId = Thief
	[25] = const.Class.Thief,	-- Merchant = Thief
	[26] = const.Class.Knight,	-- Repair = Knight
	[27] = const.Class.Knight,	-- Bodybuilding = Knight
	[28] = const.Class.Druid,	-- Meditation = Druid
	[29] = const.Class.Archer,	-- Perception = Archer
	[30] = nil,
	[31] = const.Class.Thief,	-- Disarm = Thief
	[32] = const.Class.Monk,	-- Dodging = Monk
	[33] = const.Class.Monk, 	-- Unarmed = Monk
	[34] = const.Class.Ranger,	-- Mon Id = Ranger
	[35] = const.Class.Ranger,	-- Arms = Ranger
	[36] = nil,
	[37] = const.Class.Druid,	-- Alchemy = Druid
	[38] = const.Class.Sorcerer	-- Learning = Sorcerer

}

local PeasantPromoteTopic = 1721

local function PromotePeasant(To)

	evt.ForPlayer("Current")
	if not evt.Cmp{"ClassIs", const.Class.Peasant} then
		return false
	end

	evt.Set{"ClassIs", To}
	evt.Add{"Experience", 5000}

	if To == const.Class.Vampire or To == const.Class.Nosferatu then
		local cChar = Party[Game.CurrentPlayer]
		local Gender = Game.CharacterPortraits[cChar.Face].DefSex
		local NewFace = 12 + math.random(0,1)*2 + Gender

		cChar.Face = NewFace
		SetCharFace(Game.CurrentPlayer, NewFace)
		cChar.Skills[const.Skills.VampireAbility] = 1
		cChar.Spells[110] = true

		local new_race = table.filter(Game.Races, 0,
			"BaseRace", "=", Game.Races[cChar.Attrs.Race].BaseRace,
			"Family", "=", const.RaceFamily.Vampire,
			"Mature", "=", 0
			)[1].Id
		if new_race and new_race >= 0 then
			cChar.Attrs.Race = new_race
		end

	elseif To == const.Class.DarkElf then
		local cChar = Party[Game.CurrentPlayer]
		cChar.Skills[const.Skills.DarkElfAbility] = 1
		cChar.Spells[99] = true

	end

	return true

end

local function CheckRace(To)

	local cChar = Party[Game.CurrentPlayer]
	local cRace = GetCharRace(cChar)
	local Races = const.Race

	if To == const.Class.Vampire and
		(cRace == Races.Human or cRace == Races.Elf or cRace == Races.DarkElf or cRace == Races.Goblin) then

		return true
	end

	local T = Game.CharSelection.ClassByRace[cRace]
	if T then
		return T[To]
	end

	return false

end

local CurPeasantPromClass
local RestrictedTeachers = {427, 418}
function events.EnterNPC(i)
	local cNPC = Game.NPC[i]
	for i = 0, 4 do
		if cNPC.Events[i] == PeasantPromoteTopic then
			cNPC.Events[i] = 0
		end
	end

	if table.find(RestrictedTeachers, i) then
		return
	end

	if CheckClassInParty(const.Class.Peasant) then
		local ClassId
		local cEvent
		for Eid = 0, 5 do
			cEvent = cNPC.Events[Eid]
			local TTopic = Game.TeacherTopics[cEvent]
			if TTopic and TeacherPromoters[TTopic.SId] then
				ClassId = TeacherPromoters[TTopic.SId]
			end
		end

		if not ClassId then
			return
		end

		CurPeasantPromClass = ClassId

		for i = 0, 4 do
			if cNPC.Events[i] == 0 then
				cEvent = i
				break
			end
		end

		if not cEvent then
			return
		end

		cNPC.Events[cEvent] = PeasantPromoteTopic
		Game.NPCTopic[PeasantPromoteTopic] = string.format(Game.NPCText[1676], Game.ClassNames[ClassId])
	end

end

local PeasantLastClick = 0
evt.Global[PeasantPromoteTopic] = function()
	if Game.CurrentPlayer < 0 then
		return
	end

	local ClassId = CurPeasantPromClass

	if not CheckRace(ClassId) then
		Message(string.format(Game.NPCText[1679], Game.ClassNames[ClassId]))
		return
	end

	if PeasantLastClick + 2 > os.time() then
		PeasantLastClick = 0
		if PromotePeasant(ClassId) then
			Message(string.format(Game.NPCText[1678], Game.ClassNames[ClassId]))
		end
	else
		PeasantLastClick = os.time()
		Message(string.format(Game.NPCText[1677], Game.ClassNames[ClassId]))
	end
end

--------------------------------------------
---- 	ELF/VAMPIRE/DRAGON TEACHERS		----
--------------------------------------------

local LastLearnClick = 0
local LastTeacherSkill
local LearnSkillTopic = 1674
local SkillsToLearnFromTeachers = {21,22,23}

local function PartyCanLearn(skill)
	for _,pl in Party do
		if pl.Skills[skill] == 0 and GetMaxAvailableSkill(pl, skill) > 0 then
			return true
		end
	end
	return false
end

evt.Global[LearnSkillTopic] = function()
	if Game.CurrentPlayer < 0 then
		return
	end

	local Player = Party[Game.CurrentPlayer]
	local Skill = LastTeacherSkill
	local cNPC = Game.NPC[GetCurrentNPC()]

	if not Skill then
		return
	end

	if Player.Skills[Skill] > 0 then
		Message(string.format(Game.GlobalTxt[403], Game.SkillNames[Skill]))
	elseif GetMaxAvailableSkill(Player, Skill) == 0 then
		Message(string.format(Game.GlobalTxt[632],
				GetClassName({ClassId = Player.Class, RaceId = Player.Attrs.Race})))
	elseif Party.Gold < 500 then
		Message(Game.GlobalTxt[155])
	elseif GetMaxAvailableSkill(Player, Skill) > 0 and Player.Skills[Skill] == 0 then
		evt[Game.CurrentPlayer].Add{"Experience", 0} -- animation
		Player.Skills[Skill] = 1

		for i = 9, 11 do
			local CurS, CurM = SplitSkill(Player.Skills[i+12])
			for iL = 0 + i*11, CurM + i*11 - 1 do
				Player.Spells[iL] = true
			end
		end

		evt[Game.CurrentPlayer].Subtract{"Gold", 500}
		Message(Game.GlobalTxt[569])
	end
end

function events.EnterNPC(i)

	LastTeacherSkill = nil

	local TTopic
	local cNPC = Game.NPC[i]
	for Eid = 0, 5 do
		TTopic = Game.TeacherTopics[cNPC.Events[Eid]]
		if TTopic then
			LastTeacherSkill = TTopic.SId
			break
		end
	end

	if not table.find(SkillsToLearnFromTeachers, LastTeacherSkill) then
		return
	end

	if LastTeacherSkill and PartyCanLearn(LastTeacherSkill) then
		local str = Game.GlobalTxt[534]
		str = string.replace(str, "%lu", "500")
		str = string.format(str, Game.GlobalTxt[431], Game.SkillNames[LastTeacherSkill], "")

		Game.NPCTopic[LearnSkillTopic] = str
		cNPC.Events[NPCFollowers.FindFreeEvent(cNPC, LearnSkillTopic)] = LearnSkillTopic
	else
		NPCFollowers.ClearEvents(cNPC, {LearnSkillTopic})
	end

end
